import { Component, OnInit } from '@angular/core';
import { IsService } from '../is.service';
import { FacetDefinition, CLASS_PROPERTY_KEY } from './resourceidentification';
import { Resource } from '../is-model/reference/entities/Resource';
import { TypeDefinition } from '../is-model/types/TypeDefinition';

@Component({
  selector: 'app-resource-list',
  templateUrl: './resource-list.component.html',
  styleUrls: ['./resource-list.component.css']
})
export class ResourceListComponent implements OnInit {

  constructor(private isService: IsService) { }

  set resourceType(resourceType: string) {
    // tslint:disable-next-line: no-console
    console.debug('Going to retrieve ' + resourceType + ' instances');
    this._resourceType = resourceType;
    this.isService.getResourceInstances(this._resourceType, res => { this.resources = this.analyseResources(res); });
  }

  get resourceType(): string {
    return this._resourceType;
  }

  public static facetTypes: Map<string, TypeDefinition> = new Map<string, TypeDefinition>();

  // <FacetType, Array<Resource>>
  public resources: Map<string, Array<Resource>>;

  // tslint:disable-next-line: variable-name
  private _resourceType: string;

  ngOnInit() {
    this.resourceType = 'Service';
  }

  analyseResources(resources: Resource[]): Map<string, Array<Resource>> {

    const map = new Map<string, Array<Resource>>();

    for (const resource of resources) {

      const facetType: string = resource.consistsOf[0].target[CLASS_PROPERTY_KEY];
      let res: Array<Resource>;

      if (map.has(facetType)) {
        if (!ResourceListComponent.facetTypes.has(facetType)) {
          let typeDefinition: TypeDefinition;
          this.isService.getTypeDefinition(facetType, false, types => { typeDefinition = types[0]; } );
          ResourceListComponent.facetTypes.set(facetType, typeDefinition);
        }
        res = map.get(facetType);
      } else {
        res = new Array<Resource>();
        map.set(facetType, res);
      }
      res.push(resource);
    }

    return map;

  }

}
