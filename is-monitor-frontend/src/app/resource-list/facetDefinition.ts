import { TypeDefinition } from '../is-model/types/TypeDefinition';

export const CLASS_PROPERTY_KEY = '@class';
export const SUPERCLASSES_PROPERTY_KEY = '@superClasses';
export const HEADER_PROPERTY_KEY = 'header';

export class FacetDefinition {

    public typeName: string;
    public mandatoryProperties: Set<string>;
    public typeDefinition: TypeDefinition;

    // private first = true;

    constructor(typeName: string, typeDefinition: TypeDefinition) {
        this.typeName = typeName;
        this.typeDefinition = typeDefinition;
        this.analyseType();
    }

    public analyseType() {
        this.mandatoryProperties = new Set<string>();

        for (const p of this.typeDefinition.properties) {
            switch (p.name) {

                case (CLASS_PROPERTY_KEY || SUPERCLASSES_PROPERTY_KEY || HEADER_PROPERTY_KEY) : {
                    continue;
                }

                default: {
                    if (p.mandatory) {
                        this.mandatoryProperties.add(p.name);
                    }
                }
            }

        }

        /*
        if (this.first) {
            const facet = resource.consistsOf[0].target;
            for (const propertyKey in facet) {
                if (propertyKey.localeCompare(CLASS_PROPERTY_KEY) === 0) {
                    continue;
                }
                if (propertyKey.localeCompare(SUPERCLASSES_PROPERTY_KEY) === 0) {
                    continue;
                }
                if (propertyKey.localeCompare(HEADER_PROPERTY_KEY) === 0) {
                    continue;
                }
                this.mandatoryProperties.add(propertyKey);
            }
            this.first = false;
        }
        */
    }
}
