import { Property } from './Property';

type RemoveConstraint = 'cascadeWhenOrphan' | 'cascade' | 'keep';
export const RemoveConstraint = {

    /*
     * When the source Entity is removed also the target
     * Entity is removed but if and only if the latter has no other
     * incoming Relation.
     */
    cascadeWhenOrphan: 'cascadeWhenOrphan' as RemoveConstraint,

    /*
     * When the source Entity is removed also the target
     * Entity is removed.     */
    cascade: 'cascade' as RemoveConstraint,

    /*
     * When the source Entity is removed the target Entity
     * is keep.
     */
    keep: 'keep' as RemoveConstraint

};

type AddConstraint = 'propagate' | 'unpropagate';
export const AddConstraint = {

    propagate: 'propagate' as AddConstraint,

    unpropagate: 'unpropagate' as AddConstraint

};

export class PropagationConstraint extends Property {

    remove: RemoveConstraint;
    add: AddConstraint;



    constructor(remove: RemoveConstraint, add: AddConstraint) {
        super();
        this.remove = remove;
        this.add = add;
    }

}
