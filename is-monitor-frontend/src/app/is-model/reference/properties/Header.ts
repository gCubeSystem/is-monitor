import { Property } from './Property';

export class Header extends Property {
    'uuid': string;
    'creator': string;
    'modifiedBy': string;
    'creationTime': string;
    'lastUpdateTime': string;
}
