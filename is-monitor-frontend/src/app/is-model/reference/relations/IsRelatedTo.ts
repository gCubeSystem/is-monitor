import { Resource } from '../entities/Resource';
import { Relation } from './Relation';
import { RemoveConstraint, AddConstraint, PropagationConstraint } from '../properties/PropagationConstraint';

export class IsRelatedTo<Out extends Resource, In extends Resource> extends Relation<Out, In> {

    constructor(propagationConstraint: PropagationConstraint =
        new PropagationConstraint(RemoveConstraint.keep, AddConstraint.unpropagate)) {
        super(propagationConstraint);
    }
}
