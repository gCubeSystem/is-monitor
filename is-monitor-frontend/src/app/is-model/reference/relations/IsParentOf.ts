import { Context } from '../entities/Context';
import { Relation } from './Relation';

export class IsParentOf<Out extends Context, In extends Context>
    extends Relation<Out, In> {

}
