import { Resource } from '../entities/Resource';
import { ConsistsOf } from './ConsistsOf';
import { Facet } from '../entities/Facet';

export class IsIdentifiedBy<Out extends Resource, In extends Facet> extends ConsistsOf<Out, In> {

}
