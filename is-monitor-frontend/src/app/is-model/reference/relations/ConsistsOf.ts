import { Resource } from '../entities/Resource';
import { Relation } from './Relation';
import { Facet } from '../entities/Facet';
import { PropagationConstraint, RemoveConstraint, AddConstraint } from '../properties/PropagationConstraint';

export class ConsistsOf<Out extends Resource, In extends Facet> extends Relation<Out, In> {

    constructor(propagationConstraint: PropagationConstraint =
        new PropagationConstraint(RemoveConstraint.cascade, AddConstraint.unpropagate)) {
        super(propagationConstraint);
    }

}
