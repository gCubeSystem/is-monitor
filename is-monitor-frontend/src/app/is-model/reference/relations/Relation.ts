import { Entity } from '../entities/Entity';
import { ER } from '../ER';
import { PropagationConstraint } from '../properties/PropagationConstraint';

export abstract class Relation<Out extends Entity, In extends Entity> extends ER {

    source?: Out;
    target: In;

    propagationConstraint?: PropagationConstraint;

    [x: string]: any;

    constructor(propagationConstraint?: PropagationConstraint) {
        super();
        this.propagationConstraint = propagationConstraint;
    }
}
