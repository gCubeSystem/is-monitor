import { Entity } from './Entity';
import { ConsistsOf } from '../relations/ConsistsOf';
import { IsRelatedTo } from '../relations/IsRelatedTo';
import { Facet } from './Facet';

export class Resource extends Entity {

    consistsOf?: Array<ConsistsOf<Resource, Facet>>;
    isRelatedTo?: Array<IsRelatedTo<Resource, Resource>>;

}
