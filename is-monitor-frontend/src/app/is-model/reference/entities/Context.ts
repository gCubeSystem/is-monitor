import { Entity } from './Entity';
import { IsParentOf } from '../relations/IsParentOf';

export class Context extends Entity {

    name: string;
    parent: IsParentOf<Context, Context>;
    children: Array<IsParentOf<Context, Context>>;

}
