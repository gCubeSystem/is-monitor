export class ISManageable {
    '@class': string;
    '@superClasses'?: Array<string>;
}
