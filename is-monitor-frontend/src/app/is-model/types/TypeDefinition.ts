import { PropertyDefinition } from './PropertyDefinition';

export interface TypeDefinition {
  name: string;
  description: string | null;
  abstract: boolean;
  superClasses?: Array<string>;
  properties?: Array<PropertyDefinition>;
}
