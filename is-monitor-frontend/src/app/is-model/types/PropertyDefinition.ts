enum OType {

    Boolean = 0,
    Integer = 1,
    Short = 2,
    Long = 3,
    Float = 4,
    Double = 5,
    Datetime = 6,
    String = 7,
    Bynary = 8,
    Property = 9,
    'Property List' = 10,
    'Property Set' = 11,
    'Property Map' = 12,
    Byte = 17,
    Binary = 8

}

export interface PropertyDefinition {

    name: string;
    description: string;
    mandatory: boolean;
    readonly: boolean;
    notnull: boolean;
    max: number | null;
    min: number | null;
    regexp: string | null;
    linkedType: OType | null;
    linkedClass: string | null;
    type: number | null;

}
