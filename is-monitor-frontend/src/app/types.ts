import { TypeDefinition } from './is-model/types/TypeDefinition';

export const types: TypeDefinition[] = [
    {
        name: 'Resource',
        description: null,
        superClasses: [
            'Entity'
        ],
        properties: [],
        abstract: true
    },
    {
        name: 'Service',
        description: null,
        superClasses: [
            'Resource'
        ],
        properties: [],
        abstract: true
    },
    {
        name: 'ConfigurationTemplate',
        description: null,
        superClasses: [
            'Resource'
        ],
        properties: [],
        abstract: false
    },
    {
        name: 'Dataset',
        description: null,
        superClasses: [
            'Resource'
        ],
        properties: [],
        abstract: false
    },
    {
        name: 'ConcreteDataset',
        description: null,
        superClasses: [
            'Dataset'
        ],
        properties: [],
        abstract: false
    },
    {
        name: 'HostingNode',
        description: null,
        superClasses: [
            'Service'
        ],
        properties: [],
        abstract: false
    },
    {
        name: 'VirtualService',
        description: null,
        superClasses: [
            'Service'
        ],
        properties: [],
        abstract: false
    },
    {
        name: 'EService',
        description: null,
        superClasses: [
            'Service'
        ],
        properties: [],
        abstract: false
    },
    {
        name: 'RunningPlugin',
        description: null,
        superClasses: [
            'EService'
        ],
        properties: [],
        abstract: false
    },
    {
        name: 'Software',
        description: null,
        superClasses: [
            'Resource'
        ],
        properties: [],
        abstract: false
    },
    {
        name: 'Schema',
        description: null,
        superClasses: [
            'Resource'
        ],
        properties: [],
        abstract: false
    },
    {
        name: 'Configuration',
        description: null,
        superClasses: [
            'ConfigurationTemplate'
        ],
        properties: [],
        abstract: false
    },
    {
        name: 'LegalBody',
        description: null,
        superClasses: [
            'Actor'
        ],
        properties: [],
        abstract: false
    },
    {
        name: 'VirtualMachine',
        description: null,
        superClasses: [
            'Service'
        ],
        properties: [],
        abstract: false
    },
    {
        name: 'Site',
        description: null,
        superClasses: [
            'Resource'
        ],
        properties: [],
        abstract: false
    },
    {
        name: 'Person',
        description: null,
        superClasses: [
            'Actor'
        ],
        properties: [],
        abstract: false
    },
    {
        name: 'Plugin',
        description: null,
        superClasses: [
            'Software'
        ],
        properties: [],
        abstract: false
    },
    {
        name: 'Actor',
        description: null,
        superClasses: [
            'Resource'
        ],
        properties: [],
        abstract: true
    }
];
