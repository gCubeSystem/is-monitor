import { Resource } from './is-model/reference/entities/Resource';

export const resources: Array<Resource> = [
    {
        '@class': 'HostingNode',
        header: {
            '@class': 'Header',
            uuid: 'f0460614-9ffb-4ecd-bf52-d91e8d81d604',
            creator: 'luca.frosini',
            modifiedBy: 'luca.frosini',
            creationTime: '2109-06-29 12:00:00.000 CEST',
            lastUpdateTime: '2109-06-29 12:00:00.000 CEST'
        },
        consistsOf: [
            {
                '@class': 'IsIdentifiedBy',
                header: {
                    '@class': 'Header',
                    uuid: '84d5756a-693e-4b4d-99dd-a150cf843c3e',
                    creator: 'luca.frosini',
                    modifiedBy: 'luca.frosini',
                    creationTime: '2109-06-29 12:00:00.000 CEST',
                    lastUpdateTime: '2109-06-29 12:00:00.000 CEST'
                },
                target: {
                    '@class': 'NetworkingFacet',
                    header: {
                        '@class': 'Header',
                        uuid: 'aff4173c-ef7d-4f4f-9195-6b33d852e033',
                        creator: 'luca.frosini',
                        modifiedBy: 'luca.frosini',
                        creationTime: '2109-06-29 12:00:00.000 CEST',
                        lastUpdateTime: '2109-06-29 12:00:00.000 CEST'
                    },
                    hostName: 'pc-frosini.isti.cnr.it',
                    domainName: 'isti.cnr.it',
                    mask: null,
                    broadcastAddress: null,
                    ipaddress: '127.0.1.1',
                    Port: 8080
                }
            }
        ],
        isRelatedTo: []
    },
    {
        '@class': 'EService',
        header: {
            '@class': 'Header',
            uuid: '23b6145f-b5f0-42d1-9630-17590d9831d6',
            creator: 'luca.frosini',
            modifiedBy: 'luca.frosini',
            creationTime: '2109-06-29 12:00:00.000 CEST',
            lastUpdateTime: '2109-06-29 12:00:00.000 CEST'
        },
        consistsOf: [
            {
                '@class': 'IsIdentifiedBy',
                header: {
                    '@class': 'Header',
                    uuid: '7fcff150-4007-4ad0-8618-a8f3ba83a366',
                    creator: 'luca.frosini',
                    modifiedBy: 'luca.frosini',
                    creationTime: '2109-06-29 12:00:00.000 CEST',
                    lastUpdateTime: '2109-06-29 12:00:00.000 CEST'
                },
                target: {
                    '@class': 'SoftwareFacet',
                    header: {
                        '@class': 'Header',
                        uuid: '4e664568-1669-4b38-a3db-acf1de789c96',
                        creator: 'luca.frosini',
                        modifiedBy: 'luca.frosini',
                        creationTime: '2109-06-29 12:00:00.000 CEST',
                        lastUpdateTime: '2109-06-29 12:00:00.000 CEST'
                    },
                    group: 'vre-management',
                    name: 'whn-manager',
                    version: '1.3.0'
                }
            }
        ],
        isRelatedTo: []
    }
];
