import { Resource } from './is-model/reference/entities/Resource';

export const resources: Array<Resource> = [
    {
        header: {
            '@class': 'Header',
            creator: 'luca.frosini',
            creationTime: '2019-10-18 12:38:52.161 +0200',
            modifiedBy: 'luca.frosini',
            uuid: 'd487b1a9-1433-41ac-9352-ba110e20f7a1',
            lastUpdateTime: '2019-10-18 12:38:52.161 +0200'
        },
        '@class': 'EService',
        '@superClasses': [
            'Entity',
            'Resource',
            'Service'
        ],
        consistsOf: [
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:52.092 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: 'e726d533-cd17-4833-8ea1-276c58ffd6ea',
                    lastUpdateTime: '2019-10-18 12:38:52.092 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'IsIdentifiedBy',
                '@superClasses': [
                    'Relation',
                    'ConsistsOf'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:52.161 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'd487b1a9-1433-41ac-9352-ba110e20f7a1',
                        lastUpdateTime: '2019-10-18 12:38:52.161 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    name: 'Test',
                    description: 'test',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:52.090 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '738dc11e-57d9-4ba0-9f28-b10931847b1e',
                        lastUpdateTime: '2019-10-18 12:38:52.090 +0200'
                    },
                    optional: 'false',
                    EXPORTED: 'EXPORTED_FROM_OLD_GCORE_IS',
                    version: '0.0.0-0',
                    group: 'Test',
                    '@class': 'SoftwareFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:52.099 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: 'ee9afd8d-8b1f-4e8e-8ccf-c0046ecb9c8f',
                    lastUpdateTime: '2019-10-18 12:38:52.099 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:52.161 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'd487b1a9-1433-41ac-9352-ba110e20f7a1',
                        lastUpdateTime: '2019-10-18 12:38:52.161 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    name: 'test',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:52.097 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'b0fdd581-8175-4e03-b9cd-11a21bc43dee',
                        lastUpdateTime: '2019-10-18 12:38:52.097 +0200'
                    },
                    optional: 'false',
                    group: 'PLATFORM',
                    '@class': 'SoftwareFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:52.122 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '5ea8f256-7bfe-41e6-8c63-a59c8cc37018',
                    lastUpdateTime: '2019-10-18 12:38:52.122 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:52.161 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'd487b1a9-1433-41ac-9352-ba110e20f7a1',
                        lastUpdateTime: '2019-10-18 12:38:52.161 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:52.105 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'f711afa9-a949-49ff-8f82-e35a46ebc147',
                        lastUpdateTime: '2019-10-18 12:38:52.105 +0200'
                    },
                    value: 'READY',
                    '@class': 'ServiceStateFacet',
                    '@superClasses': [
                        'Entity',
                        'StateFacet',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:52.127 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: 'ccaa78d0-bc17-4c7b-81a1-a501a0562a59',
                    lastUpdateTime: '2019-10-18 12:38:52.127 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:52.161 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'd487b1a9-1433-41ac-9352-ba110e20f7a1',
                        lastUpdateTime: '2019-10-18 12:38:52.161 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    hostName: 'd4science',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:52.124 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'd41be05c-c1b9-4c19-96c9-e8a17975f1e0',
                        lastUpdateTime: '2019-10-18 12:38:52.124 +0200'
                    },
                    '@class': 'NetworkingFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                POSITION: '0',
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:52.158 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '622858b3-72be-4579-95f3-d1c1c9900647',
                    lastUpdateTime: '2019-10-18 12:38:52.158 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:52.161 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'd487b1a9-1433-41ac-9352-ba110e20f7a1',
                        lastUpdateTime: '2019-10-18 12:38:52.161 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    endpoint: 'http://test.garr.d4science.org',
                    R: '3.1.5',
                    PASSWORD: {
                        '@class': 'Encrypted',
                        value: 'MwDldJhEXBV7c2zXKQQdTQ=='
                    },
                    entryName: 'test.garr.d4science.org',
                    description: 'AccessPoint test: fill properties',
                    USERNAME: '',
                    knime: '3.6.1',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:52.147 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'b9f028ea-b7d2-43f9-8391-eab29f1912bc',
                        lastUpdateTime: '2019-10-18 12:38:52.147 +0200'
                    },
                    user: 'roberto.cirillo',
                    '@class': 'AccessPointFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            }
        ]
    },
    {
        header: {
            '@class': 'Header',
            creator: 'luca.frosini',
            creationTime: '2019-10-18 12:38:54.282 +0200',
            modifiedBy: 'luca.frosini',
            uuid: '6613f537-86bd-400d-a1cd-33b0ce0b53b5',
            lastUpdateTime: '2019-10-18 12:38:54.282 +0200'
        },
        '@class': 'EService',
        '@superClasses': [
            'Entity',
            'Resource',
            'Service'
        ],
        consistsOf: [
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:54.191 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '9a6c93c2-d6af-4093-b794-82c76b41765a',
                    lastUpdateTime: '2019-10-18 12:38:54.191 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'IsIdentifiedBy',
                '@superClasses': [
                    'Relation',
                    'ConsistsOf'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:54.282 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '6613f537-86bd-400d-a1cd-33b0ce0b53b5',
                        lastUpdateTime: '2019-10-18 12:38:54.282 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    name: 'Apache Server',
                    description: 'An Apache Server running html-js web applications',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:54.190 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '2d8ee42b-4e36-4087-8d45-c412cfa3efb8',
                        lastUpdateTime: '2019-10-18 12:38:54.190 +0200'
                    },
                    optional: 'false',
                    EXPORTED: 'EXPORTED_FROM_OLD_GCORE_IS',
                    version: '2.4.18-0',
                    group: 'Application',
                    '@class': 'SoftwareFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:54.202 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '2b76ff9e-2d54-4218-b9b8-011fea878041',
                    lastUpdateTime: '2019-10-18 12:38:54.202 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:54.282 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '6613f537-86bd-400d-a1cd-33b0ce0b53b5',
                        lastUpdateTime: '2019-10-18 12:38:54.282 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    name: 'Apache',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:54.198 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'f0cc7d84-c2e3-4425-97cb-4d8d3166c7d5',
                        lastUpdateTime: '2019-10-18 12:38:54.198 +0200'
                    },
                    optional: 'false',
                    group: 'PLATFORM',
                    '@class': 'SoftwareFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:54.220 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: 'c48ca9c8-02ab-4d46-a26d-9e82a9dbcf86',
                    lastUpdateTime: '2019-10-18 12:38:54.220 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:54.282 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '6613f537-86bd-400d-a1cd-33b0ce0b53b5',
                        lastUpdateTime: '2019-10-18 12:38:54.282 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:54.211 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'bc1861ea-708f-40b0-8ab8-c2a893e4cc28',
                        lastUpdateTime: '2019-10-18 12:38:54.211 +0200'
                    },
                    value: '',
                    '@class': 'ServiceStateFacet',
                    '@superClasses': [
                        'Entity',
                        'StateFacet',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:54.250 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: 'd605d8e8-5688-4249-8547-97b998c355cf',
                    lastUpdateTime: '2019-10-18 12:38:54.250 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:54.282 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '6613f537-86bd-400d-a1cd-33b0ce0b53b5',
                        lastUpdateTime: '2019-10-18 12:38:54.282 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    hostName: 'access-d.d4science.org',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:54.226 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'e1420fba-c915-47a8-a4c7-a6e098c34a92',
                        lastUpdateTime: '2019-10-18 12:38:54.226 +0200'
                    },
                    '@class': 'NetworkingFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                POSITION: '0',
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:54.281 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '17eae4c0-7908-4a04-9a0a-5c13f71af3ea',
                    lastUpdateTime: '2019-10-18 12:38:54.281 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:54.282 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '6613f537-86bd-400d-a1cd-33b0ce0b53b5',
                        lastUpdateTime: '2019-10-18 12:38:54.282 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    endpoint: 'http://access-d.d4science.org',
                    PASSWORD: {
                        '@class': 'Encrypted',
                        value: 'MwDldJhEXBV7c2zXKQQdTQ=='
                    },
                    entryName: 'Apache Server',
                    description: 'Apache Server',
                    USERNAME: '',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:54.270 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '23c017dd-dd4c-4e52-ba7c-bfee59bc12cb',
                        lastUpdateTime: '2019-10-18 12:38:54.270 +0200'
                    },
                    '@class': 'AccessPointFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            }
        ]
    },
    {
        header: {
            '@class': 'Header',
            creator: 'luca.frosini',
            creationTime: '2019-10-18 12:38:56.515 +0200',
            modifiedBy: 'luca.frosini',
            uuid: 'b780c014-7191-461a-8d44-438d0d23c77e',
            lastUpdateTime: '2019-10-18 12:38:56.515 +0200'
        },
        '@class': 'EService',
        '@superClasses': [
            'Entity',
            'Resource',
            'Service'
        ],
        consistsOf: [
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:56.483 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '9c169e68-6927-4f21-9ccb-2217e73aebcb',
                    lastUpdateTime: '2019-10-18 12:38:56.483 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'IsIdentifiedBy',
                '@superClasses': [
                    'Relation',
                    'ConsistsOf'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:56.515 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'b780c014-7191-461a-8d44-438d0d23c77e',
                        lastUpdateTime: '2019-10-18 12:38:56.515 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    name: 'DataMiner',
                    description: 'DataMiner is an e-Infrastructure service providing state-of-the art DataMining algorithms and ecological modelling approaches under the Web Processing Service (WPS) standard.',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:56.481 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '0d74aefc-dda6-46d7-807b-3f1f1c0b0dda',
                        lastUpdateTime: '2019-10-18 12:38:56.481 +0200'
                    },
                    optional: 'false',
                    EXPORTED: 'EXPORTED_FROM_OLD_GCORE_IS',
                    version: '1.0.0-0',
                    group: 'DataAnalysis',
                    '@class': 'SoftwareFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:56.487 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '94392bef-7e24-4abc-9610-ccc27ab4ac55',
                    lastUpdateTime: '2019-10-18 12:38:56.487 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:56.515 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'b780c014-7191-461a-8d44-438d0d23c77e',
                        lastUpdateTime: '2019-10-18 12:38:56.515 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    name: 'DataMiner',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:56.485 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'd7d4abf9-d1fc-4794-90c5-0f4608507b42',
                        lastUpdateTime: '2019-10-18 12:38:56.485 +0200'
                    },
                    optional: 'false',
                    group: 'PLATFORM',
                    '@class': 'SoftwareFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:56.492 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: 'd26fb519-8fb2-48cb-ac4d-d37771935ee3',
                    lastUpdateTime: '2019-10-18 12:38:56.492 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:56.515 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'b780c014-7191-461a-8d44-438d0d23c77e',
                        lastUpdateTime: '2019-10-18 12:38:56.515 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:56.489 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '0a9e1c87-5e70-4a6c-bae8-07befe53fcf6',
                        lastUpdateTime: '2019-10-18 12:38:56.489 +0200'
                    },
                    value: 'READY',
                    '@class': 'ServiceStateFacet',
                    '@superClasses': [
                        'Entity',
                        'StateFacet',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:56.498 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '68d65da8-ee7b-4340-927c-e228a3bd742a',
                    lastUpdateTime: '2019-10-18 12:38:56.498 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:56.515 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'b780c014-7191-461a-8d44-438d0d23c77e',
                        lastUpdateTime: '2019-10-18 12:38:56.515 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    hostName: 'dataminer2-d-d4s.d4science.org',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:56.494 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '195889b2-7db0-4899-9aec-25494f3528c8',
                        lastUpdateTime: '2019-10-18 12:38:56.494 +0200'
                    },
                    '@class': 'NetworkingFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                POSITION: '0',
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:56.514 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: 'cdde55b1-579e-405e-85c8-4306e4211303',
                    lastUpdateTime: '2019-10-18 12:38:56.514 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:56.515 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'b780c014-7191-461a-8d44-438d0d23c77e',
                        lastUpdateTime: '2019-10-18 12:38:56.515 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    Java: '8',
                    description: 'dataminer2-d-d4s.d4science.org',
                    python3: '3.4.3',
                    python2: '2.7.6',
                    endpoint: 'http://dataminer2-d-d4s.d4science.org/wps/WebProcessingService',
                    R: '3.4.4',
                    PASSWORD: {
                        '@class': 'Encrypted',
                        value: 'MwDldJhEXBV7c2zXKQQdTQ=='
                    },
                    entryName: 'dataminer2-d-d4s.d4science.org',
                    USERNAME: '',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:56.506 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '822dbbdf-8c87-4db0-a0fd-9916ec0d51b0',
                        lastUpdateTime: '2019-10-18 12:38:56.506 +0200'
                    },
                    KNIME: '3.7.1',
                    '@class': 'AccessPointFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            }
        ]
    },
    {
        header: {
            '@class': 'Header',
            creator: 'luca.frosini',
            creationTime: '2019-10-18 12:38:58.306 +0200',
            modifiedBy: 'luca.frosini',
            uuid: '56ec4876-999f-4afc-a9e3-efbda5f5c8bc',
            lastUpdateTime: '2019-10-18 12:38:58.306 +0200'
        },
        '@class': 'EService',
        '@superClasses': [
            'Entity',
            'Resource',
            'Service'
        ],
        consistsOf: [
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:58.243 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '6eb24563-95c4-446e-8006-43c5bdc16c34',
                    lastUpdateTime: '2019-10-18 12:38:58.243 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'IsIdentifiedBy',
                '@superClasses': [
                    'Relation',
                    'ConsistsOf'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:58.306 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '56ec4876-999f-4afc-a9e3-efbda5f5c8bc',
                        lastUpdateTime: '2019-10-18 12:38:58.306 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    name: 'CKanDataCatalogue',
                    description: 'A Tomcat Server hosting the ckan data catalogue',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:58.242 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '5629c79f-bd21-4b7b-979a-45ebe6fe54bb',
                        lastUpdateTime: '2019-10-18 12:38:58.242 +0200'
                    },
                    optional: 'false',
                    EXPORTED: 'EXPORTED_FROM_OLD_GCORE_IS',
                    version: '7.0.0-0',
                    group: 'Application',
                    '@class': 'SoftwareFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:58.249 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '86132368-975c-4a49-b55b-203f12ac2a7c',
                    lastUpdateTime: '2019-10-18 12:38:58.249 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:58.306 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '56ec4876-999f-4afc-a9e3-efbda5f5c8bc',
                        lastUpdateTime: '2019-10-18 12:38:58.306 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    name: 'Tomcat',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:58.245 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '4f48a0a4-8ba1-41e3-8b67-f031d135b3b6',
                        lastUpdateTime: '2019-10-18 12:38:58.245 +0200'
                    },
                    optional: 'false',
                    group: 'PLATFORM',
                    '@class': 'SoftwareFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:58.257 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '2ad9e910-3993-4714-9b83-cdc241bfaac5',
                    lastUpdateTime: '2019-10-18 12:38:58.257 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:58.306 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '56ec4876-999f-4afc-a9e3-efbda5f5c8bc',
                        lastUpdateTime: '2019-10-18 12:38:58.306 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:58.253 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'b3fa5c2e-7f20-41fa-9f5f-c3bb8caa151f',
                        lastUpdateTime: '2019-10-18 12:38:58.253 +0200'
                    },
                    value: 'READY',
                    '@class': 'ServiceStateFacet',
                    '@superClasses': [
                        'Entity',
                        'StateFacet',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:58.274 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: 'bd48df1f-8182-41c2-8c0c-682c027e69bc',
                    lastUpdateTime: '2019-10-18 12:38:58.274 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:58.306 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '56ec4876-999f-4afc-a9e3-efbda5f5c8bc',
                        lastUpdateTime: '2019-10-18 12:38:58.306 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    hostName: 'ckan-d-d4s.d4science.org',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:58.267 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'fea23d6e-6dd9-4891-b786-0b6cfc1ec023',
                        lastUpdateTime: '2019-10-18 12:38:58.267 +0200'
                    },
                    '@class': 'NetworkingFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                POSITION: '0',
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:58.305 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '1c0a189f-eed7-4604-abb9-6b435df13976',
                    lastUpdateTime: '2019-10-18 12:38:58.305 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:58.306 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '56ec4876-999f-4afc-a9e3-efbda5f5c8bc',
                        lastUpdateTime: '2019-10-18 12:38:58.306 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    ALERT_USERS_ON_POST_CREATION: 'false',
                    SOCIAL_POST: 'true',
                    description: 'CKan Data Catalogue',
                    URL_RESOLVER: 'http://data-d.d4science.org/uri-resolver/catalogue/',
                    endpoint: 'https://ckan-d-d4s.d4science.org',
                    API_KEY: 'true',
                    PASSWORD: {
                        '@class': 'Encrypted',
                        value: 'MwDldJhEXBV7c2zXKQQdTQ=='
                    },
                    SOLR_INDEX_ADDRESS: 'https://ckan-d-d4s.d4science.org/solr/',
                    entryName: 'CKan Data Catalogue',
                    USERNAME: 'gcubeservice',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:58.290 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'dc8ddf96-6e47-44db-a507-ea973390f3db',
                        lastUpdateTime: '2019-10-18 12:38:58.290 +0200'
                    },
                    IS_MANAGE_PRODUCT_ENABLED: 'true',
                    '@class': 'AccessPointFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            }
        ]
    },
    {
        header: {
            '@class': 'Header',
            creator: 'luca.frosini',
            creationTime: '2019-10-18 12:38:59.970 +0200',
            modifiedBy: 'luca.frosini',
            uuid: 'e8c8dba4-6071-4f51-9317-0910c500fa04',
            lastUpdateTime: '2019-10-18 12:38:59.970 +0200'
        },
        '@class': 'EService',
        '@superClasses': [
            'Entity',
            'Resource',
            'Service'
        ],
        consistsOf: [
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:59.915 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '4b5f698e-9e22-4977-a29c-3423dcedd46f',
                    lastUpdateTime: '2019-10-18 12:38:59.915 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'IsIdentifiedBy',
                '@superClasses': [
                    'Relation',
                    'ConsistsOf'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:59.970 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'e8c8dba4-6071-4f51-9317-0910c500fa04',
                        lastUpdateTime: '2019-10-18 12:38:59.970 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    name: 'SAIService',
                    description: 'SAIService ',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:59.913 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '0bc4dbcc-08a0-449f-94d7-4ad644270239',
                        lastUpdateTime: '2019-10-18 12:38:59.913 +0200'
                    },
                    optional: 'false',
                    EXPORTED: 'EXPORTED_FROM_OLD_GCORE_IS',
                    version: '1.0.0-0',
                    group: 'Application',
                    '@class': 'SoftwareFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:59.922 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: 'c38eddd2-acf9-4a85-8e37-fdaa22a0ac04',
                    lastUpdateTime: '2019-10-18 12:38:59.922 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:59.970 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'e8c8dba4-6071-4f51-9317-0910c500fa04',
                        lastUpdateTime: '2019-10-18 12:38:59.970 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    name: 'SAIService',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:59.916 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '0a1668f4-4ee0-455d-acb9-9a6a599f0449',
                        lastUpdateTime: '2019-10-18 12:38:59.916 +0200'
                    },
                    optional: 'false',
                    group: 'PLATFORM',
                    '@class': 'SoftwareFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:59.929 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '0e99ca7d-2dd5-47af-9a29-a934c5d12484',
                    lastUpdateTime: '2019-10-18 12:38:59.929 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:59.970 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'e8c8dba4-6071-4f51-9317-0910c500fa04',
                        lastUpdateTime: '2019-10-18 12:38:59.970 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:59.924 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '69a6a6fa-aaca-47b9-95f9-1a5bd85c733e',
                        lastUpdateTime: '2019-10-18 12:38:59.924 +0200'
                    },
                    value: 'READY',
                    '@class': 'ServiceStateFacet',
                    '@superClasses': [
                        'Entity',
                        'StateFacet',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:59.946 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: 'a92b229c-a198-496c-acd6-f743ded4604c',
                    lastUpdateTime: '2019-10-18 12:38:59.946 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:59.970 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'e8c8dba4-6071-4f51-9317-0910c500fa04',
                        lastUpdateTime: '2019-10-18 12:38:59.970 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    hostName: 'd4science.org',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:59.937 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '4af1ffbc-2335-48e3-9ddb-4506257b4201',
                        lastUpdateTime: '2019-10-18 12:38:59.937 +0200'
                    },
                    '@class': 'NetworkingFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                POSITION: '0',
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:59.969 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '278c9fce-9989-43be-9ad7-041f75157ee0',
                    lastUpdateTime: '2019-10-18 12:38:59.969 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:59.970 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'e8c8dba4-6071-4f51-9317-0910c500fa04',
                        lastUpdateTime: '2019-10-18 12:38:59.970 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    endpoint: 'http://d4science.org',
                    PASSWORD: {
                        '@class': 'Encrypted',
                        value: 'WYiErCOwWPSIs7swGGP+d8iZJDwzUYjSShsmfDBRsdSV+SBUtuWyiHhMz3njEUV/'
                    },
                    entryName: 'SAIService',
                    description: 'SAI Infrastructure Application Token',
                    USERNAME: 'SAIService',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:59.958 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'fb5dd64b-b9e2-48ed-944b-0abba995e0d6',
                        lastUpdateTime: '2019-10-18 12:38:59.958 +0200'
                    },
                    '@class': 'AccessPointFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            }
        ]
    },
    {
        header: {
            '@class': 'Header',
            creator: 'luca.frosini',
            creationTime: '2019-10-18 12:39:01.790 +0200',
            modifiedBy: 'luca.frosini',
            uuid: '92b2cad7-f655-46bd-a9b8-30145520a84d',
            lastUpdateTime: '2019-10-18 12:39:01.790 +0200'
        },
        '@class': 'EService',
        '@superClasses': [
            'Entity',
            'Resource',
            'Service'
        ],
        consistsOf: [
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:39:01.733 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '49f3e155-e0e1-4b91-a564-c5feb7cc0166',
                    lastUpdateTime: '2019-10-18 12:39:01.733 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'IsIdentifiedBy',
                '@superClasses': [
                    'Relation',
                    'ConsistsOf'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:01.790 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '92b2cad7-f655-46bd-a9b8-30145520a84d',
                        lastUpdateTime: '2019-10-18 12:39:01.790 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    name: 'SocialNetworking',
                    description: 'The social networking service endpoint',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:01.732 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '354851b9-920e-4825-b7d5-99145169ea3a',
                        lastUpdateTime: '2019-10-18 12:39:01.732 +0200'
                    },
                    optional: 'false',
                    EXPORTED: 'EXPORTED_FROM_OLD_GCORE_IS',
                    version: '2.22.1-0',
                    group: 'Portal',
                    '@class': 'SoftwareFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:39:01.743 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '08785e82-10d9-4538-923b-896498bf3292',
                    lastUpdateTime: '2019-10-18 12:39:01.743 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:01.790 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '92b2cad7-f655-46bd-a9b8-30145520a84d',
                        lastUpdateTime: '2019-10-18 12:39:01.790 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    name: 'Jersey',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:01.741 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'f17f78fb-bc97-4cae-a3d9-f28bb43d1eb7',
                        lastUpdateTime: '2019-10-18 12:39:01.741 +0200'
                    },
                    optional: 'false',
                    group: 'PLATFORM',
                    '@class': 'SoftwareFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:39:01.757 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '3198afd2-bc8b-4bb8-9d3b-5f91fb647daf',
                    lastUpdateTime: '2019-10-18 12:39:01.757 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:01.790 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '92b2cad7-f655-46bd-a9b8-30145520a84d',
                        lastUpdateTime: '2019-10-18 12:39:01.790 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:01.750 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'bfc9db6a-e181-4da3-ba49-d8ba38daa710',
                        lastUpdateTime: '2019-10-18 12:39:01.750 +0200'
                    },
                    value: 'READY',
                    '@class': 'ServiceStateFacet',
                    '@superClasses': [
                        'Entity',
                        'StateFacet',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:39:01.773 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '1420973b-e4bf-42db-8cab-42556269001c',
                    lastUpdateTime: '2019-10-18 12:39:01.773 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:01.790 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '92b2cad7-f655-46bd-a9b8-30145520a84d',
                        lastUpdateTime: '2019-10-18 12:39:01.790 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    hostName: 'dev2.d4science.org',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:01.765 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'dd0e379e-a4f0-4750-889f-d50015f9d330',
                        lastUpdateTime: '2019-10-18 12:39:01.765 +0200'
                    },
                    '@class': 'NetworkingFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                POSITION: '0',
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:39:01.789 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '8b07d70e-df44-4f84-ace9-bea07541e5e7',
                    lastUpdateTime: '2019-10-18 12:39:01.789 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:01.790 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '92b2cad7-f655-46bd-a9b8-30145520a84d',
                        lastUpdateTime: '2019-10-18 12:39:01.790 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    endpoint: 'https://socialnetworking-d-d4s.d4science.org/social-networking-library-ws/rest/',
                    PASSWORD: {
                        '@class': 'Encrypted',
                        value: 'MwDldJhEXBV7c2zXKQQdTQ=='
                    },
                    entryName: 'SocialNetworkingService',
                    description: 'The service base path',
                    USERNAME: '',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:01.778 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '00da5809-0da3-41aa-bd6e-28b4961fe909',
                        lastUpdateTime: '2019-10-18 12:39:01.778 +0200'
                    },
                    '@class': 'AccessPointFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            }
        ]
    },
    {
        header: {
            '@class': 'Header',
            creator: 'luca.frosini',
            creationTime: '2019-10-18 12:39:03.447 +0200',
            modifiedBy: 'luca.frosini',
            uuid: '0d894b65-dfe1-40b7-810b-1aedd96371dd',
            lastUpdateTime: '2019-10-18 12:39:03.447 +0200'
        },
        '@class': 'EService',
        '@superClasses': [
            'Entity',
            'Resource',
            'Service'
        ],
        consistsOf: [
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:39:03.401 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: 'd3e49caa-5364-4bd1-acdf-71e9cd29b970',
                    lastUpdateTime: '2019-10-18 12:39:03.401 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'IsIdentifiedBy',
                '@superClasses': [
                    'Relation',
                    'ConsistsOf'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:03.447 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '0d894b65-dfe1-40b7-810b-1aedd96371dd',
                        lastUpdateTime: '2019-10-18 12:39:03.447 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    name: 'Test3',
                    description: 'Test',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:03.400 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'f2e1f3ba-45d6-4609-b80c-b5f3e3911258',
                        lastUpdateTime: '2019-10-18 12:39:03.400 +0200'
                    },
                    optional: 'false',
                    EXPORTED: 'EXPORTED_FROM_OLD_GCORE_IS',
                    version: '0.0.0-0',
                    group: 'Test',
                    '@class': 'SoftwareFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:39:03.414 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '246b242a-ed07-4dc1-a314-56542ee952f3',
                    lastUpdateTime: '2019-10-18 12:39:03.414 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:03.447 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '0d894b65-dfe1-40b7-810b-1aedd96371dd',
                        lastUpdateTime: '2019-10-18 12:39:03.447 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    name: 'TEst',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:03.409 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'b2419035-c78c-449c-82e7-8daca0aea199',
                        lastUpdateTime: '2019-10-18 12:39:03.409 +0200'
                    },
                    optional: 'false',
                    group: 'PLATFORM',
                    '@class': 'SoftwareFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:39:03.430 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '1c55fe4a-7910-41cb-8162-9021e86264a9',
                    lastUpdateTime: '2019-10-18 12:39:03.430 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:03.447 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '0d894b65-dfe1-40b7-810b-1aedd96371dd',
                        lastUpdateTime: '2019-10-18 12:39:03.447 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:03.422 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'b9c4a81f-0dd5-4475-a878-478f7436e977',
                        lastUpdateTime: '2019-10-18 12:39:03.422 +0200'
                    },
                    value: 'READY',
                    '@class': 'ServiceStateFacet',
                    '@superClasses': [
                        'Entity',
                        'StateFacet',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:39:03.446 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '7a10cb3a-61bb-4afa-b76b-6df5a37f027b',
                    lastUpdateTime: '2019-10-18 12:39:03.446 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:03.447 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '0d894b65-dfe1-40b7-810b-1aedd96371dd',
                        lastUpdateTime: '2019-10-18 12:39:03.447 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    hostName: 'D4SCIENCE.ORG',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:03.442 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '100b47e9-c2bf-4406-9022-2fb932afcf3c',
                        lastUpdateTime: '2019-10-18 12:39:03.442 +0200'
                    },
                    '@class': 'NetworkingFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            }
        ]
    },
    {
        header: {
            '@class': 'Header',
            creator: 'luca.frosini',
            creationTime: '2019-10-18 12:38:52.834 +0200',
            modifiedBy: 'luca.frosini',
            uuid: 'bdaccb35-7f27-45a6-8ca9-11d467cb9233',
            lastUpdateTime: '2019-10-18 12:38:52.834 +0200'
        },
        '@class': 'EService',
        '@superClasses': [
            'Entity',
            'Resource',
            'Service'
        ],
        consistsOf: [
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:52.525 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '23c29ccf-40dd-42af-9ffc-3bf24e17c941',
                    lastUpdateTime: '2019-10-18 12:38:52.525 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'IsIdentifiedBy',
                '@superClasses': [
                    'Relation',
                    'ConsistsOf'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:52.834 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'bdaccb35-7f27-45a6-8ca9-11d467cb9233',
                        lastUpdateTime: '2019-10-18 12:38:52.834 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    name: 'Persistence',
                    description: 'This ServiceEndpoint contains the parameter to connect to CouchBase to persist log accounting.',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:52.524 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '8764c467-4dfd-4cec-acd9-9406b2536dcc',
                        lastUpdateTime: '2019-10-18 12:38:52.524 +0200'
                    },
                    optional: 'false',
                    EXPORTED: 'EXPORTED_FROM_OLD_GCORE_IS',
                    version: '4.1.0-4908',
                    group: 'Accounting',
                    '@class': 'SoftwareFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:52.534 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '6d3c3008-0dda-41da-acf4-3662664869fc',
                    lastUpdateTime: '2019-10-18 12:38:52.534 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:52.834 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'bdaccb35-7f27-45a6-8ca9-11d467cb9233',
                        lastUpdateTime: '2019-10-18 12:38:52.834 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    name: 'CouchBase',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:52.530 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'e24feab4-c7de-4c17-a380-2beb4951e81c',
                        lastUpdateTime: '2019-10-18 12:38:52.530 +0200'
                    },
                    optional: 'false',
                    group: 'PLATFORM',
                    '@class': 'SoftwareFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:52.558 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '9ffb5b3b-4657-4caa-81e7-87867f651196',
                    lastUpdateTime: '2019-10-18 12:38:52.558 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:52.834 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'bdaccb35-7f27-45a6-8ca9-11d467cb9233',
                        lastUpdateTime: '2019-10-18 12:38:52.834 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:52.545 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'd218d0e1-a741-47f0-aaf7-0a84e4c347d5',
                        lastUpdateTime: '2019-10-18 12:38:52.545 +0200'
                    },
                    value: 'READY',
                    '@class': 'ServiceStateFacet',
                    '@superClasses': [
                        'Entity',
                        'StateFacet',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:52.582 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '75459224-da43-4f00-90aa-82eeec0effa2',
                    lastUpdateTime: '2019-10-18 12:38:52.582 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:52.834 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'bdaccb35-7f27-45a6-8ca9-11d467cb9233',
                        lastUpdateTime: '2019-10-18 12:38:52.834 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    hostName: 'http://couchbase02-d-d4s.d4science.org, http://couchbase03-d-d4s.d4science.org',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:52.566 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'a8fea1b4-f12a-491b-9d5f-c14ea3f7201f',
                        lastUpdateTime: '2019-10-18 12:38:52.566 +0200'
                    },
                    '@class': 'NetworkingFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                POSITION: '0',
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:52.618 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: 'd8588df8-fcb2-456b-ac48-7cb633dcf420',
                    lastUpdateTime: '2019-10-18 12:38:52.618 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:52.834 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'bdaccb35-7f27-45a6-8ca9-11d467cb9233',
                        lastUpdateTime: '2019-10-18 12:38:52.834 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    AggregatedStorageUsageRecord: 'StorageUsageRecord',
                    StorageStatusRecord: 'accounting_storage_status',
                    description: 'CouchBase Cluster',
                    ServiceUsageRecord: 'ServiceUsageRecord',
                    AggregationSchedulerTime: '30',
                    AggregatedServiceUsageRecord: 'ServiceUsageRecord',
                    PortletUsageRecord: 'PortletUsageRecord',
                    AggregatedPortletUsageRecord: 'PortletUsageRecord',
                    BufferRecordNumber: '1000',
                    StorageUsageRecord: 'StorageUsageRecord',
                    PASSWORD: {
                        '@class': 'Encrypted',
                        value: 'xs2D2PHnrI03miO5B3B/Xw=='
                    },
                    AggregatedJobUsageRecord: 'old_tasks_and_jobs',
                    AggregatedTaskUsageRecord: 'old_tasks_and_jobs',
                    entryName: 'PersistenceCouchBase',
                    JobUsageRecord: 'JobUsageRecord',
                    AggregatedStorageStatusRecord: 'accounting_storage_status',
                    NOT_URI_ENDPOINT: 'http://couchbase02-d-d4s.d4science.org, http://couchbase03-d-d4s.d4science.org',
                    USERNAME: '',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:52.594 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'eb99ffbd-5839-4f08-8533-c00c943b5812',
                        lastUpdateTime: '2019-10-18 12:38:52.594 +0200'
                    },
                    BufferRecordTime: '30',
                    '@class': 'AccessPointFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                POSITION: '1',
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:52.659 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '325f6fa2-7145-4ac7-87a8-fcd99f753f4d',
                    lastUpdateTime: '2019-10-18 12:38:52.659 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:52.834 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'bdaccb35-7f27-45a6-8ca9-11d467cb9233',
                        lastUpdateTime: '2019-10-18 12:38:52.834 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    AggregatedStorageUsageRecord: 'StorageUsageRecord',
                    StorageStatusRecord: 'accounting_storage_status',
                    description: 'CouchBase Cluster',
                    ServiceUsageRecord: 'ServiceUsageRecord',
                    PortletUsageRecord: 'PortletUsageRecord',
                    AggregatedServiceUsageRecord: 'ServiceUsageRecord',
                    AggregatedPortletUsageRecord: 'PortletUsageRecord',
                    StorageUsageRecord: 'StorageUsageRecord',
                    PASSWORD: {
                        '@class': 'Encrypted',
                        value: 'xs2D2PHnrI03miO5B3B/Xw=='
                    },
                    AggregatedJobUsageRecord: 'JobUsageRecord',
                    AggregatedTaskUsageRecord: 'JobUsageRecord',
                    entryName: 'AccountingPersistenceQueryCouchBase',
                    JobUsageRecord: 'JobUsageRecord',
                    AggregatedStorageStatusRecord: 'accounting_storage_status',
                    NOT_URI_ENDPOINT: 'http://couchbase02-d-d4s.d4science.org, http://couchbase03-d-d4s.d4science.org',
                    USERNAME: '',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:52.634 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '2a7b0493-fbc7-4d2a-b65e-b0e5f909dd92',
                        lastUpdateTime: '2019-10-18 12:38:52.634 +0200'
                    },
                    '@class': 'AccessPointFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                POSITION: '2',
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:52.682 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: 'fb8b3ebf-b963-4f3d-89d2-77a72d4646e5',
                    lastUpdateTime: '2019-10-18 12:38:52.682 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:52.834 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'bdaccb35-7f27-45a6-8ca9-11d467cb9233',
                        lastUpdateTime: '2019-10-18 12:38:52.834 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    endpoint: 'http://accounting-service-d.d4science.org/accounting-service',
                    PASSWORD: {
                        '@class': 'Encrypted',
                        value: 'MwDldJhEXBV7c2zXKQQdTQ=='
                    },
                    entryName: 'PersistenceAccountingService',
                    description: 'Persistence Accounting Service HAProxy address',
                    USERNAME: '',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:52.662 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '08001867-dcdb-451f-848f-78a756f4cebd',
                        lastUpdateTime: '2019-10-18 12:38:52.662 +0200'
                    },
                    '@class': 'AccessPointFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                POSITION: '3',
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:52.754 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '336c3929-0d01-4183-8582-6895249da761',
                    lastUpdateTime: '2019-10-18 12:38:52.754 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:52.834 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'bdaccb35-7f27-45a6-8ca9-11d467cb9233',
                        lastUpdateTime: '2019-10-18 12:38:52.834 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    BufferRecordNumber: '1',
                    endpoint: 'PersistenceNoInsert',
                    PASSWORD: {
                        '@class': 'Encrypted',
                        value: 'MwDldJhEXBV7c2zXKQQdTQ=='
                    },
                    entryName: 'PersistenceNoInsert',
                    description: 'Persistence No Insert',
                    USERNAME: '',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:52.703 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'a6e6cf88-e907-468c-bc7d-2dea068098bf',
                        lastUpdateTime: '2019-10-18 12:38:52.703 +0200'
                    },
                    AggregationSchedulerTime: '30',
                    BufferRecordTime: '30',
                    '@class': 'AccessPointFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                POSITION: '4',
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:52.770 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: 'cd446c7b-21a2-43bf-bcc8-defe7fa09af2',
                    lastUpdateTime: '2019-10-18 12:38:52.770 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:52.834 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'bdaccb35-7f27-45a6-8ca9-11d467cb9233',
                        lastUpdateTime: '2019-10-18 12:38:52.834 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    description: 'CouchBase Cluster',
                    'ServiceUsageRecord-DAILY-src': 'ServiceUsageRecord',
                    'ServiceUsageRecord-DAILY-dst': 'ServiceUsageRecord',
                    'StorageUsageRecord-DAILY-dst': 'StorageUsageRecord',
                    'StorageUsageRecord-DAILY-src': 'accounting_storage',
                    PASSWORD: {
                        '@class': 'Encrypted',
                        value: 'xs2D2PHnrI03miO5B3B/Xw=='
                    },
                    'StorageUsageRecord-MONTHLY-src': 'StorageUsageRecord',
                    'StorageUsageRecord-MONTHLY-dst': 'StorageUsageRecord',
                    entryName: 'AggregatorPersistence',
                    'ServiceUsageRecord-MONTHLY-src': 'ServiceUsageRecord',
                    NOT_URI_ENDPOINT: 'http://couchbase02-d-d4s.d4science.org, http://couchbase03-d-d4s.d4science.org',
                    USERNAME: '',
                    'ServiceUsageRecord-MONTHLY-dst': 'ServiceUsageRecord',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:52.757 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '4e28deaa-16be-47c7-aad3-c1504815cbcc',
                        lastUpdateTime: '2019-10-18 12:38:52.757 +0200'
                    },
                    '@class': 'AccessPointFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                POSITION: '5',
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:52.833 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: 'f358483a-b727-4577-a2da-0e7e8c59a224',
                    lastUpdateTime: '2019-10-18 12:38:52.833 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:52.834 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'bdaccb35-7f27-45a6-8ca9-11d467cb9233',
                        lastUpdateTime: '2019-10-18 12:38:52.834 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    endpoint: 'RegexRulesAggregator',
                    delay: '10',
                    PASSWORD: {
                        '@class': 'Encrypted',
                        value: 'MwDldJhEXBV7c2zXKQQdTQ=='
                    },
                    entryName: 'RegexRulesAggregator',
                    description: 'This is used to aggregate records having calledMethod which cannot be aggregated. This is normally due to auto accounted calledMethod from smartgears for RESTfull (or at least RESTlike) services which they contains the id of the resource in the URL path.',
                    USERNAME: '',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:52.778 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'f042a3b5-3c65-4ada-8d68-cd19fb3fc1af',
                        lastUpdateTime: '2019-10-18 12:38:52.778 +0200'
                    },
                    jsonArrayCalledMethodRules: '[{"regex":"/delete/[^\\\\t\\\\n\\\\r\\\\f\\\\v]+/[^\\\\t\\\\n\\\\r\\\\f\\\\v]+","replace":"/delete/{collection-id}/{item-id}","serviceClass":"Index","serviceName":"FullTextIndexNode"},{"regex":"/access/instance/Configuration/[^\\\\t\\\\n\\\\r\\\\f\\\\v]+","replace":"GET /access/instance/Configuration/{id}","serviceClass":"InformationSystem","serviceName":"resource-registry"},{"regex":"/access/instance/EService/[^\\\\t\\\\n\\\\r\\\\f\\\\v]+","replace":"GET /access/instance/EService/{id}","serviceClass":"InformationSystem","serviceName":"resource-registry"}]',
                    timeUnit: 'minutes',
                    '@class': 'AccessPointFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            }
        ]
    },
    {
        header: {
            '@class': 'Header',
            creator: 'luca.frosini',
            creationTime: '2019-10-18 12:38:54.739 +0200',
            modifiedBy: 'luca.frosini',
            uuid: 'ce7923a9-58d1-490e-a5c5-7a55e98d2681',
            lastUpdateTime: '2019-10-18 12:38:54.739 +0200'
        },
        '@class': 'EService',
        '@superClasses': [
            'Entity',
            'Resource',
            'Service'
        ],
        consistsOf: [
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:54.644 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '64f286a6-0beb-4c66-bf68-fb72b93a2979',
                    lastUpdateTime: '2019-10-18 12:38:54.644 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'IsIdentifiedBy',
                '@superClasses': [
                    'Relation',
                    'ConsistsOf'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:54.739 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'ce7923a9-58d1-490e-a5c5-7a55e98d2681',
                        lastUpdateTime: '2019-10-18 12:38:54.739 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    name: 'Thredds',
                    description: 'The Thredds NetCDF files repository',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:54.642 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '75469164-dd42-414a-a90c-a7af99c57444',
                        lastUpdateTime: '2019-10-18 12:38:54.642 +0200'
                    },
                    optional: 'false',
                    EXPORTED: 'EXPORTED_FROM_OLD_GCORE_IS',
                    version: '4.2.10-0',
                    group: 'Gis',
                    '@class': 'SoftwareFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:54.649 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: 'c2a17c37-92dc-4b6b-af58-69a2b1be05ae',
                    lastUpdateTime: '2019-10-18 12:38:54.649 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:54.739 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'ce7923a9-58d1-490e-a5c5-7a55e98d2681',
                        lastUpdateTime: '2019-10-18 12:38:54.739 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    name: 'Thredds',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:54.646 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'e1045c44-552d-4450-9cf5-0318dadc9ca8',
                        lastUpdateTime: '2019-10-18 12:38:54.646 +0200'
                    },
                    optional: 'false',
                    group: 'PLATFORM',
                    '@class': 'SoftwareFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:54.666 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: 'be8cfd91-b08e-4551-aef7-fd0e826a6e49',
                    lastUpdateTime: '2019-10-18 12:38:54.666 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:54.739 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'ce7923a9-58d1-490e-a5c5-7a55e98d2681',
                        lastUpdateTime: '2019-10-18 12:38:54.739 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:54.655 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '0983d218-6786-4bc0-9ce8-1b7da23d4fc7',
                        lastUpdateTime: '2019-10-18 12:38:54.655 +0200'
                    },
                    value: '',
                    '@class': 'ServiceStateFacet',
                    '@superClasses': [
                        'Entity',
                        'StateFacet',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:54.686 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '1adc434a-9042-451d-9bb1-d3ea3707f0b1',
                    lastUpdateTime: '2019-10-18 12:38:54.686 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:54.739 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'ce7923a9-58d1-490e-a5c5-7a55e98d2681',
                        lastUpdateTime: '2019-10-18 12:38:54.739 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    hostName: 'thredds-d-d4s.d4science.org',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:54.671 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'b0957d66-df31-4cca-9310-5c88471ae0d3',
                        lastUpdateTime: '2019-10-18 12:38:54.671 +0200'
                    },
                    '@class': 'NetworkingFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                POSITION: '0',
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:54.715 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: 'f8f84361-6def-4ada-aa40-69760d0f7c6d',
                    lastUpdateTime: '2019-10-18 12:38:54.715 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:54.739 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'ce7923a9-58d1-490e-a5c5-7a55e98d2681',
                        lastUpdateTime: '2019-10-18 12:38:54.739 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    endpoint: 'http://thredds-d-d4s.d4science.org:8080/thredds/catalog.html',
                    PASSWORD: {
                        '@class': 'Encrypted',
                        value: 'MwDldJhEXBV7c2zXKQQdTQ=='
                    },
                    entryName: 'Thredds',
                    description: 'Thredds Catalog',
                    USERNAME: '',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:54.702 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '3d6beeea-7aa7-4522-a066-b9403495c4f9',
                        lastUpdateTime: '2019-10-18 12:38:54.702 +0200'
                    },
                    '@class': 'AccessPointFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                POSITION: '1',
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:54.738 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: 'b6b95cb8-6ee1-4a86-8b47-816c1bd2364a',
                    lastUpdateTime: '2019-10-18 12:38:54.738 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:54.739 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'ce7923a9-58d1-490e-a5c5-7a55e98d2681',
                        lastUpdateTime: '2019-10-18 12:38:54.739 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    endpoint: 'https://thredds-d-d4s.d4science.org/thredds/admin/debug?catalogs/reinit',
                    PASSWORD: {
                        '@class': 'Encrypted',
                        value: 'sxgaDA6Bju8JtCysOnkPpQ=='
                    },
                    entryName: 'Remote_Management',
                    description: 'Thredds Remote Management credentials',
                    USERNAME: 'tds',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:54.724 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '51740e73-3cf0-488b-b7e7-65b68cff7e16',
                        lastUpdateTime: '2019-10-18 12:38:54.724 +0200'
                    },
                    '@class': 'AccessPointFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            }
        ]
    },
    {
        header: {
            '@class': 'Header',
            creator: 'luca.frosini',
            creationTime: '2019-10-18 12:38:56.984 +0200',
            modifiedBy: 'luca.frosini',
            uuid: '87f30102-0adc-4bf8-8864-cfc7b9c11f26',
            lastUpdateTime: '2019-10-18 12:38:56.984 +0200'
        },
        '@class': 'EService',
        '@superClasses': [
            'Entity',
            'Resource',
            'Service'
        ],
        consistsOf: [
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:56.893 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '74733de1-0211-4271-bb3d-c902471de7f5',
                    lastUpdateTime: '2019-10-18 12:38:56.893 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'IsIdentifiedBy',
                '@superClasses': [
                    'Relation',
                    'ConsistsOf'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:56.984 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '87f30102-0adc-4bf8-8864-cfc7b9c11f26',
                        lastUpdateTime: '2019-10-18 12:38:56.984 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    name: 'TwitterMonitorDatabase',
                    description: 'Database to persist Twitter Monitor Runs',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:56.891 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '11f018b7-6703-40c8-83d4-375068ab79b7',
                        lastUpdateTime: '2019-10-18 12:38:56.891 +0200'
                    },
                    optional: 'false',
                    EXPORTED: 'EXPORTED_FROM_OLD_GCORE_IS',
                    version: '9.1.0-0',
                    group: 'Database',
                    '@class': 'SoftwareFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:56.910 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: 'de10acbc-f2c8-4a42-a9bf-25556ee02517',
                    lastUpdateTime: '2019-10-18 12:38:56.910 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:56.984 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '87f30102-0adc-4bf8-8864-cfc7b9c11f26',
                        lastUpdateTime: '2019-10-18 12:38:56.984 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    name: 'postgres',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:56.901 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '87f52d3f-477f-4b39-91b1-7c09af4455a5',
                        lastUpdateTime: '2019-10-18 12:38:56.901 +0200'
                    },
                    optional: 'false',
                    group: 'PLATFORM',
                    '@class': 'SoftwareFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:56.932 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '3366e07d-7e52-49e6-a103-99e7c9d28c16',
                    lastUpdateTime: '2019-10-18 12:38:56.932 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:56.984 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '87f30102-0adc-4bf8-8864-cfc7b9c11f26',
                        lastUpdateTime: '2019-10-18 12:38:56.984 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:56.918 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'c4e740ac-b40a-42b0-af4f-60d223e782b1',
                        lastUpdateTime: '2019-10-18 12:38:56.918 +0200'
                    },
                    value: 'READY',
                    '@class': 'ServiceStateFacet',
                    '@superClasses': [
                        'Entity',
                        'StateFacet',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:56.958 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: 'aca8e145-fe09-40fc-9808-9b5f843923c2',
                    lastUpdateTime: '2019-10-18 12:38:56.958 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:56.984 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '87f30102-0adc-4bf8-8864-cfc7b9c11f26',
                        lastUpdateTime: '2019-10-18 12:38:56.984 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    hostName: 'wafi.iit.cnr.it:5433',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:56.937 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'e1e8e234-b024-452a-9b05-13cdfba5d9c7',
                        lastUpdateTime: '2019-10-18 12:38:56.937 +0200'
                    },
                    '@class': 'NetworkingFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                POSITION: '0',
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:56.983 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: 'ce185d8d-3ac2-45dc-ab9a-4d28480dbb10',
                    lastUpdateTime: '2019-10-18 12:38:56.983 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:56.984 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '87f30102-0adc-4bf8-8864-cfc7b9c11f26',
                        lastUpdateTime: '2019-10-18 12:38:56.984 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    endpoint: 'jdbc:postgresql://wafi.iit.cnr.it:5433/twmon',
                    PASSWORD: {
                        '@class': 'Encrypted',
                        value: 'mMzBscVKtEanIot2xiOE8A=='
                    },
                    entryName: 'postgress',
                    description: 'jdbc conection url',
                    USERNAME: 'twuser',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:56.967 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'd698bfc7-f972-45f3-8d95-5cc86f9d01b4',
                        lastUpdateTime: '2019-10-18 12:38:56.967 +0200'
                    },
                    '@class': 'AccessPointFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            }
        ]
    },
    {
        header: {
            '@class': 'Header',
            creator: 'luca.frosini',
            creationTime: '2019-10-18 12:38:58.717 +0200',
            modifiedBy: 'luca.frosini',
            uuid: 'ae57a069-c05a-466d-b09e-c7aecda924cc',
            lastUpdateTime: '2019-10-18 12:38:58.717 +0200'
        },
        '@class': 'EService',
        '@superClasses': [
            'Entity',
            'Resource',
            'Service'
        ],
        consistsOf: [
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:58.662 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '1bcbb043-a461-4c5d-9673-9ef330bc030a',
                    lastUpdateTime: '2019-10-18 12:38:58.662 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'IsIdentifiedBy',
                '@superClasses': [
                    'Relation',
                    'ConsistsOf'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:58.717 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'ae57a069-c05a-466d-b09e-c7aecda924cc',
                        lastUpdateTime: '2019-10-18 12:38:58.717 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    name: 'AccountingDashboard',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:58.661 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '1d244a89-0f94-45ce-810d-9fcb6390773f',
                        lastUpdateTime: '2019-10-18 12:38:58.661 +0200'
                    },
                    optional: 'false',
                    EXPORTED: 'EXPORTED_FROM_OLD_GCORE_IS',
                    version: '9.6.0-0',
                    group: 'Database',
                    '@class': 'SoftwareFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:58.674 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: 'c9e0b4a9-ec21-4c43-82e4-9461b563d50c',
                    lastUpdateTime: '2019-10-18 12:38:58.674 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:58.717 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'ae57a069-c05a-466d-b09e-c7aecda924cc',
                        lastUpdateTime: '2019-10-18 12:38:58.717 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    name: 'postgresql',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:58.670 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '3c1dd64a-eabb-44a9-8617-050545412efc',
                        lastUpdateTime: '2019-10-18 12:38:58.670 +0200'
                    },
                    optional: 'false',
                    group: 'PLATFORM',
                    '@class': 'SoftwareFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:58.694 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '3cddd2db-6a68-423a-807e-10df5c948364',
                    lastUpdateTime: '2019-10-18 12:38:58.694 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:58.717 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'ae57a069-c05a-466d-b09e-c7aecda924cc',
                        lastUpdateTime: '2019-10-18 12:38:58.717 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:58.685 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '777bfb62-dc04-4203-8186-082120b098f5',
                        lastUpdateTime: '2019-10-18 12:38:58.685 +0200'
                    },
                    value: 'READY',
                    '@class': 'ServiceStateFacet',
                    '@superClasses': [
                        'Entity',
                        'StateFacet',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:58.710 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '1e4ce5f6-33e4-498b-966a-4ea44c7f79c9',
                    lastUpdateTime: '2019-10-18 12:38:58.710 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:58.717 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'ae57a069-c05a-466d-b09e-c7aecda924cc',
                        lastUpdateTime: '2019-10-18 12:38:58.717 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    hostName: 'postgresql-srv-dev.d4science.org',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:58.697 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'f1b50985-7d3f-4ac1-b4ae-3ca4d1395034',
                        lastUpdateTime: '2019-10-18 12:38:58.697 +0200'
                    },
                    '@class': 'NetworkingFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                POSITION: '0',
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:58.716 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '39234a58-9620-48e2-bf92-b41fb2229e6c',
                    lastUpdateTime: '2019-10-18 12:38:58.716 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:58.717 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'ae57a069-c05a-466d-b09e-c7aecda924cc',
                        lastUpdateTime: '2019-10-18 12:38:58.717 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    endpoint: 'jdbc:postgresql://postgresql-srv-dev.d4science.org:5432/analytics_b_dev',
                    PASSWORD: {
                        '@class': 'Encrypted',
                        value: 'uOuwGI4babm6sNMkScnkNg=='
                    },
                    entryName: 'DatabaseParameterRetriever',
                    description: 'Database used by Accounting Dashboard Harvester Smart Executor Plugin',
                    USERNAME: 'analytics_b_dev_u',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:58.712 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'cef458cd-be98-496d-9420-3b5b20a161f2',
                        lastUpdateTime: '2019-10-18 12:38:58.712 +0200'
                    },
                    '@class': 'AccessPointFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            }
        ]
    },
    {
        header: {
            '@class': 'Header',
            creator: 'luca.frosini',
            creationTime: '2019-10-18 12:39:00.411 +0200',
            modifiedBy: 'luca.frosini',
            uuid: 'f1504591-94e9-48a9-b14a-8b98d6c3777e',
            lastUpdateTime: '2019-10-18 12:39:00.411 +0200'
        },
        '@class': 'EService',
        '@superClasses': [
            'Entity',
            'Resource',
            'Service'
        ],
        consistsOf: [
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:39:00.341 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '899001ae-59a1-4750-9f80-3806194f669e',
                    lastUpdateTime: '2019-10-18 12:39:00.341 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'IsIdentifiedBy',
                '@superClasses': [
                    'Relation',
                    'ConsistsOf'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:00.411 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'f1504591-94e9-48a9-b14a-8b98d6c3777e',
                        lastUpdateTime: '2019-10-18 12:39:00.411 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    name: 'Analytics Board',
                    description: 'Database used by Accounting Summary Portlet',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:00.340 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'acfa4841-2124-4141-b5ac-4aeed697d831',
                        lastUpdateTime: '2019-10-18 12:39:00.340 +0200'
                    },
                    optional: 'false',
                    EXPORTED: 'EXPORTED_FROM_OLD_GCORE_IS',
                    version: '9.6.0-0',
                    group: 'Database',
                    '@class': 'SoftwareFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:39:00.345 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: 'ea0fdacc-ec00-40c1-8071-a21678a3632e',
                    lastUpdateTime: '2019-10-18 12:39:00.345 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:00.411 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'f1504591-94e9-48a9-b14a-8b98d6c3777e',
                        lastUpdateTime: '2019-10-18 12:39:00.411 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    name: 'postgresql',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:00.343 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '57f6bd4c-2060-4bf2-941a-0f1d632dcc36',
                        lastUpdateTime: '2019-10-18 12:39:00.343 +0200'
                    },
                    optional: 'false',
                    group: 'PLATFORM',
                    '@class': 'SoftwareFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:39:00.362 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: 'd93a0f12-623b-4051-9098-52222ea05f4f',
                    lastUpdateTime: '2019-10-18 12:39:00.362 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:00.411 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'f1504591-94e9-48a9-b14a-8b98d6c3777e',
                        lastUpdateTime: '2019-10-18 12:39:00.411 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:00.352 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'b5ba20da-6487-46b9-98fc-4105e1d09c1f',
                        lastUpdateTime: '2019-10-18 12:39:00.352 +0200'
                    },
                    value: 'READY',
                    '@class': 'ServiceStateFacet',
                    '@superClasses': [
                        'Entity',
                        'StateFacet',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:39:00.378 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '6c566c4e-3265-4b96-a093-a7a303d581b1',
                    lastUpdateTime: '2019-10-18 12:39:00.378 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:00.411 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'f1504591-94e9-48a9-b14a-8b98d6c3777e',
                        lastUpdateTime: '2019-10-18 12:39:00.411 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    hostName: 'postgresql-srv-dev.d4science.org',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:00.365 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '1097e948-0628-4075-b3db-c54a187a3889',
                        lastUpdateTime: '2019-10-18 12:39:00.365 +0200'
                    },
                    '@class': 'NetworkingFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                POSITION: '0',
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:39:00.410 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: 'ed9c7052-3c2f-4aa0-b8c4-390df73c347a',
                    lastUpdateTime: '2019-10-18 12:39:00.410 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:00.411 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'f1504591-94e9-48a9-b14a-8b98d6c3777e',
                        lastUpdateTime: '2019-10-18 12:39:00.411 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    endpoint: 'postgresql-srv-dev.d4science.org:5432',
                    PASSWORD: {
                        '@class': 'Encrypted',
                        value: 'uOuwGI4babm6sNMkScnkNg=='
                    },
                    entryName: 'analytics_b_dev',
                    description: 'Database used by Accounting Summary Portlet',
                    USERNAME: 'analytics_b_dev_u',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:00.394 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '159b44e8-e59f-469d-a017-35700632e378',
                        lastUpdateTime: '2019-10-18 12:39:00.394 +0200'
                    },
                    '@class': 'AccessPointFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            }
        ]
    },
    {
        header: {
            '@class': 'Header',
            creator: 'luca.frosini',
            creationTime: '2019-10-18 12:39:02.226 +0200',
            modifiedBy: 'luca.frosini',
            uuid: '4d0782eb-9013-4336-8672-4ac063ff5559',
            lastUpdateTime: '2019-10-18 12:39:02.226 +0200'
        },
        '@class': 'EService',
        '@superClasses': [
            'Entity',
            'Resource',
            'Service'
        ],
        consistsOf: [
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:39:02.179 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '13b46629-a5ce-42c0-b3bc-c6516b1e49dc',
                    lastUpdateTime: '2019-10-18 12:39:02.179 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'IsIdentifiedBy',
                '@superClasses': [
                    'Relation',
                    'ConsistsOf'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:02.226 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '4d0782eb-9013-4336-8672-4ac063ff5559',
                        lastUpdateTime: '2019-10-18 12:39:02.226 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    name: 'CKanDatabase',
                    description: 'CKanDatabase',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:02.177 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '8d5b98ea-9fe4-4cf7-9cd5-02fca9df8c19',
                        lastUpdateTime: '2019-10-18 12:39:02.177 +0200'
                    },
                    optional: 'false',
                    EXPORTED: 'EXPORTED_FROM_OLD_GCORE_IS',
                    version: '9.1.0-0',
                    group: 'Database',
                    '@class': 'SoftwareFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:39:02.186 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '174ae0ec-c90d-4b40-9a54-5a91c3d806e4',
                    lastUpdateTime: '2019-10-18 12:39:02.186 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:02.226 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '4d0782eb-9013-4336-8672-4ac063ff5559',
                        lastUpdateTime: '2019-10-18 12:39:02.226 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    name: 'postgres',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:02.182 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '467080ce-b1da-4056-8e07-cea9c349c513',
                        lastUpdateTime: '2019-10-18 12:39:02.182 +0200'
                    },
                    optional: 'false',
                    group: 'PLATFORM',
                    '@class': 'SoftwareFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:39:02.196 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: 'f440fae4-5476-4292-a285-fe2efae2a192',
                    lastUpdateTime: '2019-10-18 12:39:02.196 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:02.226 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '4d0782eb-9013-4336-8672-4ac063ff5559',
                        lastUpdateTime: '2019-10-18 12:39:02.226 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:02.192 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '8a01e6cc-60b5-44f6-9d75-381804ab802f',
                        lastUpdateTime: '2019-10-18 12:39:02.192 +0200'
                    },
                    value: 'READY',
                    '@class': 'ServiceStateFacet',
                    '@superClasses': [
                        'Entity',
                        'StateFacet',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:39:02.203 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '47521149-6bad-49f0-9770-3f4489021883',
                    lastUpdateTime: '2019-10-18 12:39:02.203 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:02.226 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '4d0782eb-9013-4336-8672-4ac063ff5559',
                        lastUpdateTime: '2019-10-18 12:39:02.226 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    hostName: 'ckan-d-d4s.d4science.org',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:02.197 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '611835b8-b591-459c-b721-ebba2d8ad7bd',
                        lastUpdateTime: '2019-10-18 12:39:02.197 +0200'
                    },
                    '@class': 'NetworkingFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                POSITION: '0',
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:39:02.224 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '0f0cdf4f-5c54-4024-b69d-fca4e6117123',
                    lastUpdateTime: '2019-10-18 12:39:02.224 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:02.226 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '4d0782eb-9013-4336-8672-4ac063ff5559',
                        lastUpdateTime: '2019-10-18 12:39:02.226 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    endpoint: 'ckan-d-d4s.d4science.org:5432',
                    PASSWORD: {
                        '@class': 'Encrypted',
                        value: 'w0KVc+78b2yUQsndDh/cXyyRquuwyILTygmoF0Y5Dls='
                    },
                    entryName: 'ckan_dev',
                    description: 'ckan database access point',
                    USERNAME: 'ckan',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:02.213 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '56aa8254-c382-40bd-b7b6-7e2fbf28af74',
                        lastUpdateTime: '2019-10-18 12:39:02.213 +0200'
                    },
                    '@class': 'AccessPointFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            }
        ]
    },
    {
        header: {
            '@class': 'Header',
            creator: 'luca.frosini',
            creationTime: '2019-10-18 12:38:53.351 +0200',
            modifiedBy: 'luca.frosini',
            uuid: '687af250-ad6b-11e2-bbe7-b131fda1a8f1',
            lastUpdateTime: '2019-10-18 12:38:53.351 +0200'
        },
        '@class': 'EService',
        '@superClasses': [
            'Entity',
            'Resource',
            'Service'
        ],
        consistsOf: [
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:53.259 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '67ea9cc5-b695-4655-92c3-f2afdb11b9ff',
                    lastUpdateTime: '2019-10-18 12:38:53.259 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'IsIdentifiedBy',
                '@superClasses': [
                    'Relation',
                    'ConsistsOf'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:53.351 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '687af250-ad6b-11e2-bbe7-b131fda1a8f1',
                        lastUpdateTime: '2019-10-18 12:38:53.351 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    name: 'HTTP-URI-Resolver',
                    description: 'HTTP URI Resolver',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:53.258 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'f342d0d4-a764-4bc5-8e6c-e54730a142fa',
                        lastUpdateTime: '2019-10-18 12:38:53.258 +0200'
                    },
                    optional: 'false',
                    EXPORTED: 'EXPORTED_FROM_OLD_GCORE_IS',
                    version: '6.0.0-0',
                    group: 'Service',
                    '@class': 'SoftwareFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:53.266 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '165fe5ee-afca-4de8-99a6-dfa4252101a8',
                    lastUpdateTime: '2019-10-18 12:38:53.266 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:53.351 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '687af250-ad6b-11e2-bbe7-b131fda1a8f1',
                        lastUpdateTime: '2019-10-18 12:38:53.351 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    name: 'tomcat',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:53.261 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'fbbff5ca-678b-4851-a196-89ea50b37aee',
                        lastUpdateTime: '2019-10-18 12:38:53.261 +0200'
                    },
                    optional: 'false',
                    group: 'PLATFORM',
                    '@class': 'SoftwareFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:53.283 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '768d7401-ca76-496e-bb65-c1669343f5ed',
                    lastUpdateTime: '2019-10-18 12:38:53.283 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:53.351 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '687af250-ad6b-11e2-bbe7-b131fda1a8f1',
                        lastUpdateTime: '2019-10-18 12:38:53.351 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:53.274 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'af21170a-ca77-44f0-8e5a-3c78e2259693',
                        lastUpdateTime: '2019-10-18 12:38:53.274 +0200'
                    },
                    value: 'READY',
                    '@class': 'ServiceStateFacet',
                    '@superClasses': [
                        'Entity',
                        'StateFacet',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:53.304 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '047c64d4-5d81-4c38-9a66-2ae563a9f716',
                    lastUpdateTime: '2019-10-18 12:38:53.304 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:53.351 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '687af250-ad6b-11e2-bbe7-b131fda1a8f1',
                        lastUpdateTime: '2019-10-18 12:38:53.351 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    hostName: 'data1-d.d4science.org',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:53.290 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '41ce63fd-f97b-40c8-9a29-1097a6349c27',
                        lastUpdateTime: '2019-10-18 12:38:53.290 +0200'
                    },
                    '@class': 'NetworkingFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                POSITION: '0',
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:53.329 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '8f5fae10-caf7-4ac5-8f99-941bceda023a',
                    lastUpdateTime: '2019-10-18 12:38:53.329 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:53.351 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '687af250-ad6b-11e2-bbe7-b131fda1a8f1',
                        lastUpdateTime: '2019-10-18 12:38:53.351 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    endpoint: 'http://data1-d.d4science.org/smp',
                    PASSWORD: {
                        '@class': 'Encrypted',
                        value: 'MwDldJhEXBV7c2zXKQQdTQ=='
                    },
                    contentType_parameter: 'contentType',
                    entryName: 'smp',
                    description: 'SMP URI Resolver',
                    USERNAME: '',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:53.310 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '46b3ee80-32ea-43d4-b2e6-4a928e0137c4',
                        lastUpdateTime: '2019-10-18 12:38:53.310 +0200'
                    },
                    fileName_parameter: 'fileName',
                    SMP_URI_parameter: 'smp-uri',
                    '@class': 'AccessPointFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                POSITION: '1',
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:53.350 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '48b034ae-d400-4597-8049-8974b3417795',
                    lastUpdateTime: '2019-10-18 12:38:53.350 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:53.351 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '687af250-ad6b-11e2-bbe7-b131fda1a8f1',
                        lastUpdateTime: '2019-10-18 12:38:53.351 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    SMP_ID_parameter: 'smp-id',
                    endpoint: 'http://data1-d.d4science.org/id',
                    PASSWORD: {
                        '@class': 'Encrypted',
                        value: 'MwDldJhEXBV7c2zXKQQdTQ=='
                    },
                    contentType_parameter: 'contentType',
                    entryName: 'smp-id',
                    description: 'Storage ID URI Resolver',
                    USERNAME: '',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:53.336 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'ca446f33-3c72-41ea-a6ca-418b189e380b',
                        lastUpdateTime: '2019-10-18 12:38:53.336 +0200'
                    },
                    fileName_parameter: 'fileName',
                    '@class': 'AccessPointFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            }
        ]
    },
    {
        header: {
            '@class': 'Header',
            creator: 'luca.frosini',
            creationTime: '2019-10-18 12:38:55.670 +0200',
            modifiedBy: 'luca.frosini',
            uuid: '270292b0-e24f-11e2-9605-9e1431a7fd69',
            lastUpdateTime: '2019-10-18 12:38:55.670 +0200'
        },
        '@class': 'EService',
        '@superClasses': [
            'Entity',
            'Resource',
            'Service'
        ],
        consistsOf: [
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:55.553 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '59f24b0c-a70e-4007-a690-db55501c2958',
                    lastUpdateTime: '2019-10-18 12:38:55.553 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'IsIdentifiedBy',
                '@superClasses': [
                    'Relation',
                    'ConsistsOf'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:55.670 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '270292b0-e24f-11e2-9605-9e1431a7fd69',
                        lastUpdateTime: '2019-10-18 12:38:55.670 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    name: 'HTTP-URL-Shortener',
                    description: 'HTTP URL Shortener. The Google URL Shortener at goo.gl is a service that takes long URLs and squeezes them into fewer characters.',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:55.552 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '96b5a7c5-0e92-45c7-920f-9990f03214c6',
                        lastUpdateTime: '2019-10-18 12:38:55.552 +0200'
                    },
                    optional: 'false',
                    EXPORTED: 'EXPORTED_FROM_OLD_GCORE_IS',
                    version: '0.0.0-0',
                    group: 'Service',
                    '@class': 'SoftwareFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:55.564 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '2c7c4295-3af2-4515-b12a-2679e240753a',
                    lastUpdateTime: '2019-10-18 12:38:55.564 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:55.670 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '270292b0-e24f-11e2-9605-9e1431a7fd69',
                        lastUpdateTime: '2019-10-18 12:38:55.670 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    name: 'unknown',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:55.562 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '2548da3c-5837-49f5-bca1-4e547347edac',
                        lastUpdateTime: '2019-10-18 12:38:55.562 +0200'
                    },
                    optional: 'false',
                    group: 'PLATFORM',
                    '@class': 'SoftwareFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:55.585 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '01d7aba9-a315-4a85-ae64-ec650673ef2d',
                    lastUpdateTime: '2019-10-18 12:38:55.585 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:55.670 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '270292b0-e24f-11e2-9605-9e1431a7fd69',
                        lastUpdateTime: '2019-10-18 12:38:55.670 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:55.581 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '4c20e853-4b3d-4599-8f41-5dc4f32c4757',
                        lastUpdateTime: '2019-10-18 12:38:55.581 +0200'
                    },
                    value: 'READY',
                    '@class': 'ServiceStateFacet',
                    '@superClasses': [
                        'Entity',
                        'StateFacet',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:55.602 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '5f6633b1-8c86-4677-a42c-90586943c666',
                    lastUpdateTime: '2019-10-18 12:38:55.602 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:55.670 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '270292b0-e24f-11e2-9605-9e1431a7fd69',
                        lastUpdateTime: '2019-10-18 12:38:55.670 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    hostName: 'http://goo.gl/',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:55.593 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '1e8ad069-c5c1-45bf-b957-10899b02a530',
                        lastUpdateTime: '2019-10-18 12:38:55.593 +0200'
                    },
                    '@class': 'NetworkingFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                POSITION: '0',
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:55.669 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '53c40a32-225d-46cd-9894-367678a39422',
                    lastUpdateTime: '2019-10-18 12:38:55.669 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:55.670 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '270292b0-e24f-11e2-9605-9e1431a7fd69',
                        lastUpdateTime: '2019-10-18 12:38:55.670 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    endpoint: 'https://www.googleapis.com/urlshortener/v1/url',
                    PASSWORD: {
                        '@class': 'Encrypted',
                        value: 'MwDldJhEXBV7c2zXKQQdTQ=='
                    },
                    entryName: 'googleshortener',
                    description: 'Google HTTP URL Shortener',
                    USERNAME: '',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:55.614 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '5d9c642f-9863-40be-8c20-4215de95741b',
                        lastUpdateTime: '2019-10-18 12:38:55.614 +0200'
                    },
                    key: 'AIzaSyCQSY7UU3xiBqqlzU5ovr-efs6EXKz5e7Y',
                    '@class': 'AccessPointFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            }
        ]
    },
    {
        header: {
            '@class': 'Header',
            creator: 'luca.frosini',
            creationTime: '2019-10-18 12:38:57.434 +0200',
            modifiedBy: 'luca.frosini',
            uuid: '657bb109-cd80-4a15-9549-be225b7cc2bf',
            lastUpdateTime: '2019-10-18 12:38:57.434 +0200'
        },
        '@class': 'EService',
        '@superClasses': [
            'Entity',
            'Resource',
            'Service'
        ],
        consistsOf: [
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:57.359 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '558ae819-999b-43d8-bfcb-882c95b93789',
                    lastUpdateTime: '2019-10-18 12:38:57.359 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'IsIdentifiedBy',
                '@superClasses': [
                    'Relation',
                    'ConsistsOf'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:57.434 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '657bb109-cd80-4a15-9549-be225b7cc2bf',
                        lastUpdateTime: '2019-10-18 12:38:57.434 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    name: 'GRSF Updater',
                    description: 'The service that allows GRSF administrators to update the status of GRSF records',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:57.357 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '34745f56-c24f-404d-8407-f3e77ca5dd4a',
                        lastUpdateTime: '2019-10-18 12:38:57.357 +0200'
                    },
                    optional: 'false',
                    EXPORTED: 'EXPORTED_FROM_OLD_GCORE_IS',
                    version: '7.0.0-0',
                    group: 'Service',
                    '@class': 'SoftwareFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:57.370 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: 'ab6e8e87-2f3a-42c6-aae1-319b6f6b9c2a',
                    lastUpdateTime: '2019-10-18 12:38:57.370 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:57.434 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '657bb109-cd80-4a15-9549-be225b7cc2bf',
                        lastUpdateTime: '2019-10-18 12:38:57.434 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    name: 'Tomcat',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:57.365 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '9cf39be9-586a-43c9-b882-6a86b2779c78',
                        lastUpdateTime: '2019-10-18 12:38:57.365 +0200'
                    },
                    optional: 'false',
                    group: 'PLATFORM',
                    '@class': 'SoftwareFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:57.391 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: 'a000563d-699d-42d1-a630-6b0413388f4f',
                    lastUpdateTime: '2019-10-18 12:38:57.391 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:57.434 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '657bb109-cd80-4a15-9549-be225b7cc2bf',
                        lastUpdateTime: '2019-10-18 12:38:57.434 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:57.377 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'e0ccb7da-1237-4c3a-9f29-c616d689e5e5',
                        lastUpdateTime: '2019-10-18 12:38:57.377 +0200'
                    },
                    value: 'READY',
                    '@class': 'ServiceStateFacet',
                    '@superClasses': [
                        'Entity',
                        'StateFacet',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:57.410 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '2cafb6a5-416a-4d64-a24f-28902e964274',
                    lastUpdateTime: '2019-10-18 12:38:57.410 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:57.434 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '657bb109-cd80-4a15-9549-be225b7cc2bf',
                        lastUpdateTime: '2019-10-18 12:38:57.434 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    hostName: '83.212.113.65:8080',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:57.402 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '56c24552-0a11-419f-a3ef-e22cb2b7f78b',
                        lastUpdateTime: '2019-10-18 12:38:57.402 +0200'
                    },
                    '@class': 'NetworkingFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                POSITION: '0',
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:57.433 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '1cbdf6d2-c57e-40f9-b601-b5cef2f4e07f',
                    lastUpdateTime: '2019-10-18 12:38:57.433 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:57.434 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '657bb109-cd80-4a15-9549-be225b7cc2bf',
                        lastUpdateTime: '2019-10-18 12:38:57.434 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    endpoint: 'http://83.212.113.65:8080/grsf-services-updater-1.3.0-SNAPSHOT',
                    PASSWORD: {
                        '@class': 'Encrypted',
                        value: 'MwDldJhEXBV7c2zXKQQdTQ=='
                    },
                    entryName: 'GRSF Update service',
                    description: 'The base url of the service',
                    USERNAME: '',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:57.421 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '8e4c10d6-ead3-4eef-9491-811c8ea2dd07',
                        lastUpdateTime: '2019-10-18 12:38:57.421 +0200'
                    },
                    '@class': 'AccessPointFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            }
        ]
    },
    {
        header: {
            '@class': 'Header',
            creator: 'luca.frosini',
            creationTime: '2019-10-18 12:38:59.135 +0200',
            modifiedBy: 'luca.frosini',
            uuid: 'a8219a39-b384-4801-9d1c-3ceb4dcd3a31',
            lastUpdateTime: '2019-10-18 12:38:59.135 +0200'
        },
        '@class': 'EService',
        '@superClasses': [
            'Entity',
            'Resource',
            'Service'
        ],
        consistsOf: [
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:59.085 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '323d2a4e-8068-4324-8dca-5f4274320680',
                    lastUpdateTime: '2019-10-18 12:38:59.085 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'IsIdentifiedBy',
                '@superClasses': [
                    'Relation',
                    'ConsistsOf'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:59.135 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'a8219a39-b384-4801-9d1c-3ceb4dcd3a31',
                        lastUpdateTime: '2019-10-18 12:38:59.135 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    name: 'RConnector',
                    description: 'Service endpoint for RConnector service for RStudio dev',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:59.082 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '161394ed-537b-423a-9616-6e4b4c3f3aae',
                        lastUpdateTime: '2019-10-18 12:38:59.082 +0200'
                    },
                    optional: 'false',
                    EXPORTED: 'EXPORTED_FROM_OLD_GCORE_IS',
                    version: '0.0.0-0',
                    group: 'DataAnalysis',
                    '@class': 'SoftwareFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:59.098 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '99886ec2-d726-4364-8898-9a56c2f086a0',
                    lastUpdateTime: '2019-10-18 12:38:59.098 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:59.135 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'a8219a39-b384-4801-9d1c-3ceb4dcd3a31',
                        lastUpdateTime: '2019-10-18 12:38:59.135 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    name: 'RStudio',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:59.094 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '3e36f3e8-e369-449a-a4a9-1a9533aa91c2',
                        lastUpdateTime: '2019-10-18 12:38:59.094 +0200'
                    },
                    optional: 'false',
                    group: 'PLATFORM',
                    '@class': 'SoftwareFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:59.114 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '3dbdddf3-d89c-4921-808a-460caf8e7f10',
                    lastUpdateTime: '2019-10-18 12:38:59.114 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:59.135 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'a8219a39-b384-4801-9d1c-3ceb4dcd3a31',
                        lastUpdateTime: '2019-10-18 12:38:59.135 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:59.105 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '318e7e47-6af2-45ed-ba45-d5cdd6e9b1a7',
                        lastUpdateTime: '2019-10-18 12:38:59.105 +0200'
                    },
                    value: 'READY',
                    '@class': 'ServiceStateFacet',
                    '@superClasses': [
                        'Entity',
                        'StateFacet',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:59.134 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: 'b2d4b40a-a415-4c40-a18c-ee3c097669a5',
                    lastUpdateTime: '2019-10-18 12:38:59.134 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:59.135 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'a8219a39-b384-4801-9d1c-3ceb4dcd3a31',
                        lastUpdateTime: '2019-10-18 12:38:59.135 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    hostName: 'rstudio-dev.d4science.org:80',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:59.123 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'd5c071d4-3ef4-48ef-83ff-bb8a010b3e1d',
                        lastUpdateTime: '2019-10-18 12:38:59.123 +0200'
                    },
                    '@class': 'NetworkingFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            }
        ]
    },
    {
        header: {
            '@class': 'Header',
            creator: 'luca.frosini',
            creationTime: '2019-10-18 12:39:00.880 +0200',
            modifiedBy: 'luca.frosini',
            uuid: '82cebf60-aad9-4a66-86aa-7fa80fac0857',
            lastUpdateTime: '2019-10-18 12:39:00.880 +0200'
        },
        '@class': 'EService',
        '@superClasses': [
            'Entity',
            'Resource',
            'Service'
        ],
        consistsOf: [
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:39:00.765 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '28343b12-5b17-4e90-b1d2-feb2ece85f3d',
                    lastUpdateTime: '2019-10-18 12:39:00.765 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'IsIdentifiedBy',
                '@superClasses': [
                    'Relation',
                    'ConsistsOf'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:00.880 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '82cebf60-aad9-4a66-86aa-7fa80fac0857',
                        lastUpdateTime: '2019-10-18 12:39:00.880 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    name: 'SDMXRegistry',
                    description: 'SDMX Registry',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:00.764 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '62bb9b13-5c7a-4b69-a1e1-682e5fdeb1a3',
                        lastUpdateTime: '2019-10-18 12:39:00.764 +0200'
                    },
                    optional: 'false',
                    EXPORTED: 'EXPORTED_FROM_OLD_GCORE_IS',
                    version: '7.2.3-0',
                    group: 'SDMX',
                    '@class': 'SoftwareFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:39:00.774 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '3cafa179-0ae1-4b48-bb5e-dd5a518d5157',
                    lastUpdateTime: '2019-10-18 12:39:00.774 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:00.880 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '82cebf60-aad9-4a66-86aa-7fa80fac0857',
                        lastUpdateTime: '2019-10-18 12:39:00.880 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    name: 'Fusion Registry',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:00.769 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '98474205-5d0a-4362-a7f0-f87194720f30',
                        lastUpdateTime: '2019-10-18 12:39:00.769 +0200'
                    },
                    optional: 'false',
                    group: 'PLATFORM',
                    '@class': 'SoftwareFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:39:00.789 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: 'a230155f-5546-4f1b-8f36-a7ad349302ea',
                    lastUpdateTime: '2019-10-18 12:39:00.789 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:00.880 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '82cebf60-aad9-4a66-86aa-7fa80fac0857',
                        lastUpdateTime: '2019-10-18 12:39:00.880 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:00.781 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '599c3c02-608a-47a2-bb0c-173007850e5b',
                        lastUpdateTime: '2019-10-18 12:39:00.781 +0200'
                    },
                    value: 'READY',
                    '@class': 'ServiceStateFacet',
                    '@superClasses': [
                        'Entity',
                        'StateFacet',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:39:00.807 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '7652f280-7e31-410c-b817-e7424bcfa028',
                    lastUpdateTime: '2019-10-18 12:39:00.807 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:00.880 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '82cebf60-aad9-4a66-86aa-7fa80fac0857',
                        lastUpdateTime: '2019-10-18 12:39:00.880 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    hostName: 'fusionregistry-d4s.d4science.org',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:00.797 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '363a79df-487a-42ad-83cf-610c069ea719',
                        lastUpdateTime: '2019-10-18 12:39:00.797 +0200'
                    },
                    '@class': 'NetworkingFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                POSITION: '0',
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:39:00.830 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '541af67b-252c-4338-8c65-2aa5321c0201',
                    lastUpdateTime: '2019-10-18 12:39:00.830 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:00.880 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '82cebf60-aad9-4a66-86aa-7fa80fac0857',
                        lastUpdateTime: '2019-10-18 12:39:00.880 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    endpoint: 'https://fusionregistry-d4s.d4science.org/FusionRegistry/ws/rest/',
                    PASSWORD: {
                        '@class': 'Encrypted',
                        value: 'VjhyUZg1Ft2vgUqVoRiGEg=='
                    },
                    entryName: 'RESTV2_1',
                    description: 'REST Interface v2.1',
                    USERNAME: 'tabulardata',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:00.817 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'f2692499-fb4c-4983-9d9e-c93012bc467b',
                        lastUpdateTime: '2019-10-18 12:39:00.817 +0200'
                    },
                    '@class': 'AccessPointFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                POSITION: '1',
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:39:00.857 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '80dbecb8-49de-46b7-8bb7-b52e5e3552e3',
                    lastUpdateTime: '2019-10-18 12:39:00.857 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:00.880 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '82cebf60-aad9-4a66-86aa-7fa80fac0857',
                        lastUpdateTime: '2019-10-18 12:39:00.880 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    endpoint: 'https://fusionregistry-d4s.d4science.org/FusionRegistry/ws/rest/',
                    PASSWORD: {
                        '@class': 'Encrypted',
                        value: 'VjhyUZg1Ft2vgUqVoRiGEg=='
                    },
                    entryName: 'RESTV2',
                    description: 'REST Interface v2',
                    USERNAME: 'tabulardata',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:00.837 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '476a2b04-0b19-49d7-8729-cef704bd0ea3',
                        lastUpdateTime: '2019-10-18 12:39:00.837 +0200'
                    },
                    '@class': 'AccessPointFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                POSITION: '2',
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:39:00.879 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '19191634-d5b0-42de-ac21-e64484a4b4aa',
                    lastUpdateTime: '2019-10-18 12:39:00.879 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:00.880 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '82cebf60-aad9-4a66-86aa-7fa80fac0857',
                        lastUpdateTime: '2019-10-18 12:39:00.880 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    endpoint: 'https://fusionregistry-d4s.d4science.org/FusionRegistry/ws/rest/',
                    PASSWORD: {
                        '@class': 'Encrypted',
                        value: 'VjhyUZg1Ft2vgUqVoRiGEg=='
                    },
                    entryName: 'RESTV1',
                    description: 'REST Interface v1',
                    USERNAME: 'tabulardata',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:00.861 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'b55bfddd-8247-4e05-af3f-554689fdba89',
                        lastUpdateTime: '2019-10-18 12:39:00.861 +0200'
                    },
                    '@class': 'AccessPointFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            }
        ]
    },
    {
        header: {
            '@class': 'Header',
            creator: 'luca.frosini',
            creationTime: '2019-10-18 12:39:02.626 +0200',
            modifiedBy: 'luca.frosini',
            uuid: '3361c368-c34b-46e6-b45e-d23cc2699353',
            lastUpdateTime: '2019-10-18 12:39:02.626 +0200'
        },
        '@class': 'EService',
        '@superClasses': [
            'Entity',
            'Resource',
            'Service'
        ],
        consistsOf: [
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:39:02.586 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '9580cb0c-f82a-47d1-804a-224655d73186',
                    lastUpdateTime: '2019-10-18 12:39:02.586 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'IsIdentifiedBy',
                '@superClasses': [
                    'Relation',
                    'ConsistsOf'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:02.626 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '3361c368-c34b-46e6-b45e-d23cc2699353',
                        lastUpdateTime: '2019-10-18 12:39:02.626 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    name: 'SmartExecutor',
                    description: 'Smart Executor Service',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:02.584 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'e1c81dbd-717d-4d15-a9e9-33c034486250',
                        lastUpdateTime: '2019-10-18 12:39:02.584 +0200'
                    },
                    optional: 'false',
                    EXPORTED: 'EXPORTED_FROM_OLD_GCORE_IS',
                    version: '1.10.0-0',
                    group: 'VREManagement',
                    '@class': 'SoftwareFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:39:02.588 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '946ee489-c1ca-4342-bab4-48a10caf74b6',
                    lastUpdateTime: '2019-10-18 12:39:02.588 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:02.626 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '3361c368-c34b-46e6-b45e-d23cc2699353',
                        lastUpdateTime: '2019-10-18 12:39:02.626 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    name: 'is-exporter-d.dev.d4science.org:80',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:02.587 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '08035d48-5d9b-47c5-98c3-d58de5fd590a',
                        lastUpdateTime: '2019-10-18 12:39:02.587 +0200'
                    },
                    optional: 'false',
                    group: 'PLATFORM',
                    '@class': 'SoftwareFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:39:02.602 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '799d2f46-bd0f-4732-acc4-82bd71e0bd9f',
                    lastUpdateTime: '2019-10-18 12:39:02.602 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:02.626 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '3361c368-c34b-46e6-b45e-d23cc2699353',
                        lastUpdateTime: '2019-10-18 12:39:02.626 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:02.594 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '1c1b5377-5531-45aa-962b-7153bcf6c67f',
                        lastUpdateTime: '2019-10-18 12:39:02.594 +0200'
                    },
                    value: 'online',
                    '@class': 'ServiceStateFacet',
                    '@superClasses': [
                        'Entity',
                        'StateFacet',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:39:02.612 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '967d212f-4956-4d83-870b-d1049ce6f7a8',
                    lastUpdateTime: '2019-10-18 12:39:02.612 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:02.626 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '3361c368-c34b-46e6-b45e-d23cc2699353',
                        lastUpdateTime: '2019-10-18 12:39:02.626 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    hostName: 'is-exporter-d.dev.d4science.org:80',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:02.605 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '18c92e84-1a7f-4271-89a2-4c446ea6b5f1',
                        lastUpdateTime: '2019-10-18 12:39:02.605 +0200'
                    },
                    '@class': 'NetworkingFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                POSITION: '0',
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:39:02.625 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: 'b9c225a4-627b-4226-aa02-978af2fb17c1',
                    lastUpdateTime: '2019-10-18 12:39:02.625 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:02.626 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '3361c368-c34b-46e6-b45e-d23cc2699353',
                        lastUpdateTime: '2019-10-18 12:39:02.626 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    Version: '1.0.0',
                    entryName: 'ISExporter',
                    description: 'IS Exporter',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:02.617 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'e8407116-53d1-4373-8b57-6856d57d34ed',
                        lastUpdateTime: '2019-10-18 12:39:02.617 +0200'
                    },
                    '@class': 'AccessPointFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            }
        ]
    },
    {
        header: {
            '@class': 'Header',
            creator: 'luca.frosini',
            creationTime: '2019-10-18 12:38:53.815 +0200',
            modifiedBy: 'luca.frosini',
            uuid: '57f2590c-dbdc-4245-a805-a8d389769c03',
            lastUpdateTime: '2019-10-18 12:38:53.815 +0200'
        },
        '@class': 'EService',
        '@superClasses': [
            'Entity',
            'Resource',
            'Service'
        ],
        consistsOf: [
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:53.739 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '45c5bc4c-b7f4-4381-90f9-6bfbde65291b',
                    lastUpdateTime: '2019-10-18 12:38:53.739 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'IsIdentifiedBy',
                '@superClasses': [
                    'Relation',
                    'ConsistsOf'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:53.815 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '57f2590c-dbdc-4245-a805-a8d389769c03',
                        lastUpdateTime: '2019-10-18 12:38:53.815 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    name: 'StatisticalManagerDataBaseNEW',
                    description: 'Statistical Manager database access',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:53.738 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'f5a70893-421f-43fe-ab36-2538ffacb3ce',
                        lastUpdateTime: '2019-10-18 12:38:53.738 +0200'
                    },
                    optional: 'false',
                    EXPORTED: 'EXPORTED_FROM_OLD_GCORE_IS',
                    version: '9.1.1-1',
                    group: 'Database',
                    '@class': 'SoftwareFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:53.751 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: 'cd64f5c3-7b16-4209-90b6-f74f09578700',
                    lastUpdateTime: '2019-10-18 12:38:53.751 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:53.815 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '57f2590c-dbdc-4245-a805-a8d389769c03',
                        lastUpdateTime: '2019-10-18 12:38:53.815 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    name: 'postgres',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:53.748 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '3eb4e47e-bab9-43b5-873e-edacb2087412',
                        lastUpdateTime: '2019-10-18 12:38:53.748 +0200'
                    },
                    optional: 'false',
                    group: 'PLATFORM',
                    '@class': 'SoftwareFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:53.762 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '32c8ba26-328d-409f-84c3-43c28ce0d203',
                    lastUpdateTime: '2019-10-18 12:38:53.762 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:53.815 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '57f2590c-dbdc-4245-a805-a8d389769c03',
                        lastUpdateTime: '2019-10-18 12:38:53.815 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:53.757 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '9b230a1c-95ac-4505-a19c-dee9bc5271b4',
                        lastUpdateTime: '2019-10-18 12:38:53.757 +0200'
                    },
                    value: 'Ready',
                    '@class': 'ServiceStateFacet',
                    '@superClasses': [
                        'Entity',
                        'StateFacet',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:53.781 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '1a39b69d-71fe-474b-b11c-062e557f8115',
                    lastUpdateTime: '2019-10-18 12:38:53.781 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:53.815 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '57f2590c-dbdc-4245-a805-a8d389769c03',
                        lastUpdateTime: '2019-10-18 12:38:53.815 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    hostName: 'postgresql-srv-dev.d5science.org',
                    ghnID: 'N/A',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:53.765 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '2a84952d-96d7-4390-bacd-8fd91ddb74b0',
                        lastUpdateTime: '2019-10-18 12:38:53.765 +0200'
                    },
                    '@class': 'NetworkingFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                POSITION: '0',
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:53.789 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '8f634c2f-0c0e-4715-acfd-d7c8e0686c6e',
                    lastUpdateTime: '2019-10-18 12:38:53.789 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:53.815 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '57f2590c-dbdc-4245-a805-a8d389769c03',
                        lastUpdateTime: '2019-10-18 12:38:53.815 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    endpoint: 'jdbc:postgresql://postgresql-srv-dev.d5science.org/sm_dataspace_devnext',
                    PASSWORD: {
                        '@class': 'Encrypted',
                        value: '7/MGegYzf7hzZqCr/7xX8w=='
                    },
                    entryName: 'hibernate',
                    description: 'ciao',
                    USERNAME: 'utente',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:53.784 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '87c6f9de-496b-4e5a-9daf-8bf23ff455aa',
                        lastUpdateTime: '2019-10-18 12:38:53.784 +0200'
                    },
                    '@class': 'AccessPointFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                POSITION: '1',
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:53.814 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '097d8c99-63fe-46bd-9316-17f8ee71e34d',
                    lastUpdateTime: '2019-10-18 12:38:53.814 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:53.815 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '57f2590c-dbdc-4245-a805-a8d389769c03',
                        lastUpdateTime: '2019-10-18 12:38:53.815 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    endpoint: 'jdbc:postgresql://postgresql-srv-dev.d5science.org/sm_operation_devnext',
                    PASSWORD: {
                        '@class': 'Encrypted',
                        value: '7/MGegYzf7hzZqCr/7xX8w=='
                    },
                    entryName: 'jdbc',
                    description: 'asdfg',
                    USERNAME: 'utente',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:53.796 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '91d2bd41-13a3-4caf-a397-530cfd0dd0e4',
                        lastUpdateTime: '2019-10-18 12:38:53.796 +0200'
                    },
                    '@class': 'AccessPointFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            }
        ]
    },
    {
        header: {
            '@class': 'Header',
            creator: 'luca.frosini',
            creationTime: '2019-10-18 12:38:56.138 +0200',
            modifiedBy: 'luca.frosini',
            uuid: '4ae7dd06-ccdc-4e49-b313-ffa7b88433db',
            lastUpdateTime: '2019-10-18 12:38:56.138 +0200'
        },
        '@class': 'EService',
        '@superClasses': [
            'Entity',
            'Resource',
            'Service'
        ],
        consistsOf: [
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:56.051 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '251d82a2-9073-4afa-a309-e7cbc811be2b',
                    lastUpdateTime: '2019-10-18 12:38:56.051 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'IsIdentifiedBy',
                '@superClasses': [
                    'Relation',
                    'ConsistsOf'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:56.138 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '4ae7dd06-ccdc-4e49-b313-ffa7b88433db',
                        lastUpdateTime: '2019-10-18 12:38:56.138 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    name: 'SmartExecutorPersistenceConfiguration',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:56.049 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '00dbde61-10ba-46a8-ba48-b48e84312798',
                        lastUpdateTime: '2019-10-18 12:38:56.049 +0200'
                    },
                    optional: 'false',
                    EXPORTED: 'EXPORTED_FROM_OLD_GCORE_IS',
                    version: '1.5.0-0',
                    group: 'VREManagement',
                    '@class': 'SoftwareFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:56.063 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '604f554f-afed-41f2-8cfa-eef9e7454163',
                    lastUpdateTime: '2019-10-18 12:38:56.063 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:56.138 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '4ae7dd06-ccdc-4e49-b313-ffa7b88433db',
                        lastUpdateTime: '2019-10-18 12:38:56.138 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    name: 'SmartExecutor',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:56.057 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '9ff79fe7-87c7-42d7-85b9-113f3a8da564',
                        lastUpdateTime: '2019-10-18 12:38:56.057 +0200'
                    },
                    optional: 'false',
                    group: 'PLATFORM',
                    '@class': 'SoftwareFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:56.085 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: 'd448a861-1302-470b-b36a-e0c258e35f3f',
                    lastUpdateTime: '2019-10-18 12:38:56.085 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:56.138 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '4ae7dd06-ccdc-4e49-b313-ffa7b88433db',
                        lastUpdateTime: '2019-10-18 12:38:56.138 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:56.070 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '49e6cf30-18c1-476d-8a72-c8804ddba31d',
                        lastUpdateTime: '2019-10-18 12:38:56.070 +0200'
                    },
                    value: 'READY',
                    '@class': 'ServiceStateFacet',
                    '@superClasses': [
                        'Entity',
                        'StateFacet',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:56.106 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: 'c0c5ee63-a8db-427c-b8e2-2d056546ab9d',
                    lastUpdateTime: '2019-10-18 12:38:56.106 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:56.138 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '4ae7dd06-ccdc-4e49-b313-ffa7b88433db',
                        lastUpdateTime: '2019-10-18 12:38:56.138 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    hostName: 'd4science.org',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:56.094 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '7c17eeeb-2281-4392-b23c-a11c1b76d810',
                        lastUpdateTime: '2019-10-18 12:38:56.094 +0200'
                    },
                    '@class': 'NetworkingFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                POSITION: '0',
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:56.136 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '4837b0d2-8ddb-4862-ab3c-d51484fc03cb',
                    lastUpdateTime: '2019-10-18 12:38:56.136 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:56.138 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '4ae7dd06-ccdc-4e49-b313-ffa7b88433db',
                        lastUpdateTime: '2019-10-18 12:38:56.138 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    endpoint: 'remote:orientdb01-d-d4s.d4science.org;orientdb02-d-d4s.d4science.org;orientdb03-d-d4s.d4science.org/smartexecutor',
                    PASSWORD: {
                        '@class': 'Encrypted',
                        value: 'xkQp6n0/ye+CrpaqAc4qXTMA5XSYRFwVe3Ns1ykEHU0='
                    },
                    entryName: 'OrientDBPersistenceConnector',
                    description: 'OrientDB Cluster',
                    USERNAME: 'smartexecutor',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:56.114 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'a24b2c37-ccf5-4a0a-8f1a-dc80aa1fdc7b',
                        lastUpdateTime: '2019-10-18 12:38:56.114 +0200'
                    },
                    '@class': 'AccessPointFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            }
        ]
    },
    {
        header: {
            '@class': 'Header',
            creator: 'luca.frosini',
            creationTime: '2019-10-18 12:38:57.876 +0200',
            modifiedBy: 'luca.frosini',
            uuid: '4ecf4957-13b9-48cd-8bec-857592de6827',
            lastUpdateTime: '2019-10-18 12:38:57.876 +0200'
        },
        '@class': 'EService',
        '@superClasses': [
            'Entity',
            'Resource',
            'Service'
        ],
        consistsOf: [
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:57.798 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '77c464d8-1a70-451d-a6fb-dd7adb5ced44',
                    lastUpdateTime: '2019-10-18 12:38:57.798 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'IsIdentifiedBy',
                '@superClasses': [
                    'Relation',
                    'ConsistsOf'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:57.876 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '4ecf4957-13b9-48cd-8bec-857592de6827',
                        lastUpdateTime: '2019-10-18 12:38:57.876 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    name: 'TimeSeriesDataStore',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:57.796 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '46062c0c-d723-4ce1-bf87-f0def5de0af2',
                        lastUpdateTime: '2019-10-18 12:38:57.796 +0200'
                    },
                    optional: 'false',
                    EXPORTED: 'EXPORTED_FROM_OLD_GCORE_IS',
                    version: '8.4.0-0',
                    group: 'Gis',
                    '@class': 'SoftwareFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:57.814 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '3e71677f-0739-498e-a909-135f3a0cb0a4',
                    lastUpdateTime: '2019-10-18 12:38:57.814 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:57.876 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '4ecf4957-13b9-48cd-8bec-857592de6827',
                        lastUpdateTime: '2019-10-18 12:38:57.876 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    name: 'postgres',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:57.809 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '627d309f-8a10-4dbb-94bc-38a872361bfa',
                        lastUpdateTime: '2019-10-18 12:38:57.809 +0200'
                    },
                    optional: 'false',
                    group: 'PLATFORM',
                    '@class': 'SoftwareFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:57.830 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '0d25cd7e-14a5-471d-a8b3-779e391108fe',
                    lastUpdateTime: '2019-10-18 12:38:57.830 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:57.876 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '4ecf4957-13b9-48cd-8bec-857592de6827',
                        lastUpdateTime: '2019-10-18 12:38:57.876 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:57.821 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'c7ba2a86-7a70-47fb-958d-07494dbccaa5',
                        lastUpdateTime: '2019-10-18 12:38:57.821 +0200'
                    },
                    value: 'READY',
                    '@class': 'ServiceStateFacet',
                    '@superClasses': [
                        'Entity',
                        'StateFacet',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:57.835 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '318f4d8b-5ca4-4596-b327-a29e0ea60b82',
                    lastUpdateTime: '2019-10-18 12:38:57.835 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:57.876 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '4ecf4957-13b9-48cd-8bec-857592de6827',
                        lastUpdateTime: '2019-10-18 12:38:57.876 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    hostName: 'geoserver-test.d4science-ii.research-infrastructures.eu',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:57.832 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'c5687110-7889-4ffd-8595-4ba66eda7816',
                        lastUpdateTime: '2019-10-18 12:38:57.832 +0200'
                    },
                    '@class': 'NetworkingFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                POSITION: '0',
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:57.875 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: 'fb41ebe7-b860-446f-9457-c17c379a6d0c',
                    lastUpdateTime: '2019-10-18 12:38:57.875 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:57.876 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '4ecf4957-13b9-48cd-8bec-857592de6827',
                        lastUpdateTime: '2019-10-18 12:38:57.876 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    endpoint: 'jdbc:postgresql://geoserver-test.d4science-ii.research-infrastructures.eu/timeseriesgisdb',
                    PASSWORD: {
                        '@class': 'Encrypted',
                        value: 'h1CEFCp6oqVen072ooXX8w=='
                    },
                    entryName: 'jdbc',
                    description: 'jdbc Access',
                    USERNAME: 'postgres',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:57.858 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '5cb30c4d-fa84-4fcb-9fe9-e324c1689ec0',
                        lastUpdateTime: '2019-10-18 12:38:57.858 +0200'
                    },
                    '@class': 'AccessPointFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            }
        ]
    },
    {
        header: {
            '@class': 'Header',
            creator: 'luca.frosini',
            creationTime: '2019-10-18 12:38:59.570 +0200',
            modifiedBy: 'luca.frosini',
            uuid: '46083b40-a5e3-11e2-a210-9a433747c17a',
            lastUpdateTime: '2019-10-18 12:38:59.570 +0200'
        },
        '@class': 'EService',
        '@superClasses': [
            'Entity',
            'Resource',
            'Service'
        ],
        consistsOf: [
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:59.485 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: 'd003235b-7c44-4d78-86c4-ab429d6c8135',
                    lastUpdateTime: '2019-10-18 12:38:59.485 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'IsIdentifiedBy',
                '@superClasses': [
                    'Relation',
                    'ConsistsOf'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:59.570 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '46083b40-a5e3-11e2-a210-9a433747c17a',
                        lastUpdateTime: '2019-10-18 12:38:59.570 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    name: 'StorageManager',
                    description: 'Backend description  for StorageManager library',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:59.484 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '244b4df4-6388-4424-b9ca-9e35201ca7a8',
                        lastUpdateTime: '2019-10-18 12:38:59.484 +0200'
                    },
                    optional: 'false',
                    EXPORTED: 'EXPORTED_FROM_OLD_GCORE_IS',
                    version: '2.2.1-0',
                    group: 'DataStorage',
                    '@class': 'SoftwareFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:59.488 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '790533f2-558c-446b-a3e8-b5925cc505e7',
                    lastUpdateTime: '2019-10-18 12:38:59.488 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:59.570 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '46083b40-a5e3-11e2-a210-9a433747c17a',
                        lastUpdateTime: '2019-10-18 12:38:59.570 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    name: 'storage-manager',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:59.487 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '260924a1-57ed-4d8f-a411-cbb8f42bc3eb',
                        lastUpdateTime: '2019-10-18 12:38:59.487 +0200'
                    },
                    optional: 'false',
                    group: 'PLATFORM',
                    '@class': 'SoftwareFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:59.495 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '333d4dbd-8bc3-4afc-a982-1b7968ab4d99',
                    lastUpdateTime: '2019-10-18 12:38:59.495 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:59.570 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '46083b40-a5e3-11e2-a210-9a433747c17a',
                        lastUpdateTime: '2019-10-18 12:38:59.570 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:59.489 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '8f8d7873-e0b4-4590-8828-5f8012e0c82e',
                        lastUpdateTime: '2019-10-18 12:38:59.489 +0200'
                    },
                    value: 'READY',
                    '@class': 'ServiceStateFacet',
                    '@superClasses': [
                        'Entity',
                        'StateFacet',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:59.502 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: 'd486709f-209f-4210-8d02-aea50f57d0a2',
                    lastUpdateTime: '2019-10-18 12:38:59.502 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:59.570 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '46083b40-a5e3-11e2-a210-9a433747c17a',
                        lastUpdateTime: '2019-10-18 12:38:59.570 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    hostName: 'data.gcube.org',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:59.497 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'b42a600b-af42-458a-9bc4-dc3d52fdfa5a',
                        lastUpdateTime: '2019-10-18 12:38:59.497 +0200'
                    },
                    '@class': 'NetworkingFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                POSITION: '0',
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:59.527 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: 'b17889b4-25a2-479b-a14f-21d3da5a9bbf',
                    lastUpdateTime: '2019-10-18 12:38:59.527 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:59.570 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '46083b40-a5e3-11e2-a210-9a433747c17a',
                        lastUpdateTime: '2019-10-18 12:38:59.570 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    description: 'MongoDB server',
                    volatile: 'mongo-d-vol.d4science.org',
                    collection: 'remotefs',
                    priority: '1',
                    type: 'MongoDB',
                    PassPhrase: 'true',
                    endpoint: 'mongo3-d-d4s.d4science.org',
                    PASSWORD: {
                        '@class': 'Encrypted',
                        value: 'JFjUbdyq/7XAg0HiSPS7yQ=='
                    },
                    entryName: 'server1',
                    write_concern: '1',
                    host: 'mongo3-d-d4s.d4science.org',
                    USERNAME: 'devUser',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:59.511 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'a0eaa2a9-788f-4963-be4f-cc6614993b6d',
                        lastUpdateTime: '2019-10-18 12:38:59.511 +0200'
                    },
                    accounting_user: 'oplogger',
                    read_preference: 'PrimaryPreferred',
                    accounting_pwd: 'true',
                    '@class': 'AccessPointFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                POSITION: '1',
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:38:59.569 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: 'f644b3ce-0a21-4aad-afc0-88af9df72e83',
                    lastUpdateTime: '2019-10-18 12:38:59.569 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:59.570 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '46083b40-a5e3-11e2-a210-9a433747c17a',
                        lastUpdateTime: '2019-10-18 12:38:59.570 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    description: 'MongoDB server',
                    volatile: 'mongo-d-vol.d4science.org',
                    collection: 'remotefs',
                    priority: '1',
                    type: 'MongoDB',
                    PassPhrase: 'true',
                    endpoint: 'mongo4-d-d4s.d4science.org',
                    PASSWORD: {
                        '@class': 'Encrypted',
                        value: 'JFjUbdyq/7XAg0HiSPS7yQ=='
                    },
                    entryName: 'server2',
                    write_concern: '1',
                    host: 'mongo4-d-d4s.d4science.org',
                    USERNAME: 'devUser',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:38:59.538 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '612de519-e4db-4be2-baab-7a4e37021d24',
                        lastUpdateTime: '2019-10-18 12:38:59.538 +0200'
                    },
                    read_preference: 'PrimaryPreferred',
                    '@class': 'AccessPointFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            }
        ]
    },
    {
        header: {
            '@class': 'Header',
            creator: 'luca.frosini',
            creationTime: '2019-10-18 12:39:01.347 +0200',
            modifiedBy: 'luca.frosini',
            uuid: 'a4d83f0d-e9e5-44a4-8b25-a78378d1633c',
            lastUpdateTime: '2019-10-18 12:39:01.347 +0200'
        },
        '@class': 'EService',
        '@superClasses': [
            'Entity',
            'Resource',
            'Service'
        ],
        consistsOf: [
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:39:01.250 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: 'f3ca3f9c-4395-4217-b4de-2c7c1c3077bb',
                    lastUpdateTime: '2019-10-18 12:39:01.250 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'IsIdentifiedBy',
                '@superClasses': [
                    'Relation',
                    'ConsistsOf'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:01.347 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'a4d83f0d-e9e5-44a4-8b25-a78378d1633c',
                        lastUpdateTime: '2019-10-18 12:39:01.347 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    name: 'StatisticalManagerDataBase',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:01.247 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '394205cd-26b0-4973-a798-c100ae7e0c0c',
                        lastUpdateTime: '2019-10-18 12:39:01.247 +0200'
                    },
                    optional: 'false',
                    EXPORTED: 'EXPORTED_FROM_OLD_GCORE_IS',
                    version: '8.4.0-0',
                    group: 'Database',
                    '@class': 'SoftwareFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:39:01.256 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: 'cb90ac0e-9c0f-4963-b205-dc5d48e3db02',
                    lastUpdateTime: '2019-10-18 12:39:01.256 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:01.347 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'a4d83f0d-e9e5-44a4-8b25-a78378d1633c',
                        lastUpdateTime: '2019-10-18 12:39:01.347 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    name: 'postgres',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:01.254 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'd4180678-6474-4a3f-88c0-fc0abe2797f2',
                        lastUpdateTime: '2019-10-18 12:39:01.254 +0200'
                    },
                    optional: 'false',
                    group: 'PLATFORM',
                    '@class': 'SoftwareFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:39:01.274 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '81855427-fbec-4e98-95d5-adce29711291',
                    lastUpdateTime: '2019-10-18 12:39:01.274 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:01.347 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'a4d83f0d-e9e5-44a4-8b25-a78378d1633c',
                        lastUpdateTime: '2019-10-18 12:39:01.347 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:01.266 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '9323c05c-f270-43d2-888c-074bb7b310bd',
                        lastUpdateTime: '2019-10-18 12:39:01.266 +0200'
                    },
                    value: 'READY',
                    '@class': 'ServiceStateFacet',
                    '@superClasses': [
                        'Entity',
                        'StateFacet',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:39:01.298 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '00660825-ecf8-4482-9c36-b759a507a756',
                    lastUpdateTime: '2019-10-18 12:39:01.298 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:01.347 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'a4d83f0d-e9e5-44a4-8b25-a78378d1633c',
                        lastUpdateTime: '2019-10-18 12:39:01.347 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    hostName: 'postgresql-srv-dev.d4science.org',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:01.282 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '026e1ec5-390c-4af2-bfe3-426c9e424c8a',
                        lastUpdateTime: '2019-10-18 12:39:01.282 +0200'
                    },
                    '@class': 'NetworkingFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                POSITION: '0',
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:39:01.307 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '0d95aaad-ba8d-474a-869d-e5e770a7bc4e',
                    lastUpdateTime: '2019-10-18 12:39:01.307 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:01.347 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'a4d83f0d-e9e5-44a4-8b25-a78378d1633c',
                        lastUpdateTime: '2019-10-18 12:39:01.347 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    endpoint: 'jdbc:postgresql://postgresql-srv-dev.d4science.org/dataSpace',
                    PASSWORD: {
                        '@class': 'Encrypted',
                        value: '7/MGegYzf7hzZqCr/7xX8w=='
                    },
                    driver: 'org.postgresql.Driver',
                    entryName: 'jdbc',
                    description: 'jdbc conection url',
                    USERNAME: 'utente',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:01.302 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '679897e6-c8b8-495e-9e93-17004ea8b3de',
                        lastUpdateTime: '2019-10-18 12:39:01.302 +0200'
                    },
                    '@class': 'AccessPointFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                POSITION: '1',
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:39:01.346 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: 'dc05fc38-a1b0-4c3d-ba90-c3097bd9557f',
                    lastUpdateTime: '2019-10-18 12:39:01.346 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:01.347 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'a4d83f0d-e9e5-44a4-8b25-a78378d1633c',
                        lastUpdateTime: '2019-10-18 12:39:01.347 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    endpoint: 'jdbc:postgresql://postgresql-srv-dev.d4science.org/dataSpace',
                    PASSWORD: {
                        '@class': 'Encrypted',
                        value: '7/MGegYzf7hzZqCr/7xX8w=='
                    },
                    entryName: 'hibernate',
                    description: 'Hibernate connection url',
                    USERNAME: 'utente',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:01.310 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'd591d513-6b88-42e5-a6d8-baed27fbfd7b',
                        lastUpdateTime: '2019-10-18 12:39:01.310 +0200'
                    },
                    '@class': 'AccessPointFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            }
        ]
    },
    {
        header: {
            '@class': 'Header',
            creator: 'luca.frosini',
            creationTime: '2019-10-18 12:39:03.039 +0200',
            modifiedBy: 'luca.frosini',
            uuid: 'c36f404c-e206-416b-a967-2d3eaa14d2ff',
            lastUpdateTime: '2019-10-18 12:39:03.039 +0200'
        },
        '@class': 'EService',
        '@superClasses': [
            'Entity',
            'Resource',
            'Service'
        ],
        consistsOf: [
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:39:02.984 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '0913bd97-5046-492c-9990-bcd43035a2be',
                    lastUpdateTime: '2019-10-18 12:39:02.984 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'IsIdentifiedBy',
                '@superClasses': [
                    'Relation',
                    'ConsistsOf'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:03.039 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'c36f404c-e206-416b-a967-2d3eaa14d2ff',
                        lastUpdateTime: '2019-10-18 12:39:03.039 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    name: 'TestCopyScope',
                    description: 'Just a test ',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:02.983 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '6ccd7767-4004-4ec1-90d6-a9de50fb2e9d',
                        lastUpdateTime: '2019-10-18 12:39:02.983 +0200'
                    },
                    optional: 'false',
                    EXPORTED: 'EXPORTED_FROM_OLD_GCORE_IS',
                    version: '4.14.0-0',
                    group: 'Test',
                    '@class': 'SoftwareFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:39:02.994 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: 'f5037c30-4542-43c7-95e9-c4e33682c7a9',
                    lastUpdateTime: '2019-10-18 12:39:02.994 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:03.039 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'c36f404c-e206-416b-a967-2d3eaa14d2ff',
                        lastUpdateTime: '2019-10-18 12:39:03.039 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    name: 'd4science',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:02.989 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '89e430dd-0fa2-40fd-b242-8dbf7911d380',
                        lastUpdateTime: '2019-10-18 12:39:02.989 +0200'
                    },
                    optional: 'false',
                    group: 'PLATFORM',
                    '@class': 'SoftwareFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:39:03.018 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '9558b016-0c04-41ae-a164-b250ed89e1fe',
                    lastUpdateTime: '2019-10-18 12:39:03.018 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:03.039 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'c36f404c-e206-416b-a967-2d3eaa14d2ff',
                        lastUpdateTime: '2019-10-18 12:39:03.039 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:03.009 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: '083aaa4d-adfc-49c8-bf5e-439a03b6cd3a',
                        lastUpdateTime: '2019-10-18 12:39:03.009 +0200'
                    },
                    value: 'READY',
                    '@class': 'ServiceStateFacet',
                    '@superClasses': [
                        'Entity',
                        'StateFacet',
                        'Facet'
                    ]
                }
            },
            {
                header: {
                    '@class': 'Header',
                    creator: 'luca.frosini',
                    creationTime: '2019-10-18 12:39:03.038 +0200',
                    modifiedBy: 'luca.frosini',
                    uuid: '6107b80c-333f-44ae-9ad7-3800f22e56f6',
                    lastUpdateTime: '2019-10-18 12:39:03.038 +0200'
                },
                propagationConstraint: {
                    '@class': 'PropagationConstraint',
                    add: 'propagate',
                    remove: 'cascadeWhenOrphan'
                },
                '@class': 'ConsistsOf',
                '@superClasses': [
                    'Relation'
                ],
                source: {
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:03.039 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'c36f404c-e206-416b-a967-2d3eaa14d2ff',
                        lastUpdateTime: '2019-10-18 12:39:03.039 +0200'
                    },
                    '@class': 'EService',
                    '@superClasses': [
                        'Entity',
                        'Resource',
                        'Service'
                    ]
                },
                target: {
                    hostName: 'd4science.org',
                    header: {
                        '@class': 'Header',
                        creator: 'luca.frosini',
                        creationTime: '2019-10-18 12:39:03.021 +0200',
                        modifiedBy: 'luca.frosini',
                        uuid: 'e7df7de9-c662-47e1-91f4-3853be1f3737',
                        lastUpdateTime: '2019-10-18 12:39:03.021 +0200'
                    },
                    '@class': 'NetworkingFacet',
                    '@superClasses': [
                        'Entity',
                        'Facet'
                    ]
                }
            }
        ]
    }
];
