import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { TypeDefinition } from './is-model/types/TypeDefinition';
import { types } from './types';
import { Resource } from './is-model/reference/entities/Resource';
import { resources } from './resources';

@Injectable({
  providedIn: 'root'
})
export class IsService {

  constructor(private httpClient: HttpClient) { }

  private baseURL = 'http://pc-frosini.isti.cnr.it:8080/resource-registry';
  private typesURL = this.baseURL + '/types';
  private instancesURL = this.baseURL + '/instances';
  // private queryURL = this.baseURL + '/query';


  /* NextNext Token */
  private token = '7c66c94c-7f6e-49cd-9a34-909cd3832f3e-98187548';

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(`${operation} failed: ${error.message}`);
      if (result) {
        console.warn(`${operation} is going to provide hard-coded data which is better than nothing.`);
      }
      return of(result as T);
    };
  }

  public getTypeDefinition(typeName: string, polymorphic: boolean = true, callback: (isTypes: TypeDefinition[]) => void): void {
    const url = this.typesURL + '/' + typeName + '?polymorphic=' + polymorphic + '&gcube-token=' + this.token;
    // const observable: Observable<ISType[]> = of(types);
    const observable: Observable<TypeDefinition[]> = this.httpClient.get<TypeDefinition[]>(url);
    observable.pipe(
      catchError(this.handleError<TypeDefinition[]>('getResourceTypes()', types))
    ).subscribe(data => callback(data));
  }

  public getResourceInstances(resourceType: string, callback: (instances: Resource[]) => void): void {
    const url = this.instancesURL + '/' +  resourceType + '?polymorphic=true&gcube-token=' + this.token;
    // const observable: Observable<ISType[]> = of(types);
    const observable: Observable<any[]> = this.httpClient.get<any[]>(url);
    observable.pipe(
      catchError(this.handleError<any[]>('getResourceInstances()', resources))
    ).subscribe(data => callback(data));
  }

}
