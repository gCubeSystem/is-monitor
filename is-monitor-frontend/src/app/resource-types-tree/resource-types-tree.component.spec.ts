import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResourceTypesTreeComponent } from './resource-types-tree.component';

describe('ResourceTypesTreeComponent', () => {
  let component: ResourceTypesTreeComponent;
  let fixture: ComponentFixture<ResourceTypesTreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResourceTypesTreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResourceTypesTreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
