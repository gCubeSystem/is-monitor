import { TypeDefinition } from '../is-model/types/TypeDefinition';

export class Type {

  name: string;
  description: string;
  abstract: boolean;
  parent: Type;
  children: Type[];

  constructor(isType: TypeDefinition) {
    this.name = isType.name;
    this.description = isType.description;
    this.abstract = isType.abstract;
    this.children = new Array();
  }

  addChild(type: Type) {
    type.parent = this;
    this.children.push(type);
    this.children.sort((left, right) => left.name.localeCompare(right.name));
  }

}
