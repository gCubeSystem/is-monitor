import { Component, OnInit } from '@angular/core';
import { IsService } from '../is.service';
import { Type } from './type';
import { TypeDefinition } from '../is-model/types/TypeDefinition';

@Component({
  selector: 'app-resource-types-tree',
  templateUrl: './resource-types-tree.component.html',
  styleUrls: ['./resource-types-tree.component.css']
})
export class ResourceTypesTreeComponent implements OnInit {

  resources: Type[];

  constructor(private isService: IsService) {

  }

  ngOnInit() {
    this.isService.getTypeDefinition('Resource', true, isTypes => { this.resources = this.analyseTypes(isTypes); });
  }

  analyseUnassociated(type: Type, map: Map<string, Type>, unassociated: Map<string, TypeDefinition>) {
    const associated: string[] = new Array();

    for (const name of unassociated.keys()) {
      const t = unassociated.get(name);
      const parent = t.superClasses[0];
      if (parent === type.name) {
        type.addChild(map.get(t.name));
        associated.push(t.name);
      }
    }

    for (const a of associated) {
      unassociated.delete(a);
    }

  }

  analyseType(isType: TypeDefinition, map: Map<string, Type>, unassociated: Map<string, TypeDefinition>) {

    const type: Type = new Type(isType);

    map.set(type.name, type);

    if (type.name !== 'Resource') {
      const parent = isType.superClasses[0];
      if (map.has(parent)) {
        map.get(parent).addChild(type);
      } else {
        this.analyseUnassociated(type, map, unassociated);
        unassociated.set(type.name, isType);
      }
    }
  }

  analyseTypes(isTypes: TypeDefinition[]): Type[] {

    const map = new Map<string, Type>();
    const unassociated = new Map<string, TypeDefinition>();

    for (const isType of isTypes) {
      this.analyseType(isType, map, unassociated);
    }

    return map.get('Resource').children;
  }

}



